//
//  PlacesController.m
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "PlacesController.h"
#import "ContainerViewController.h"
#import "MainModel.h"

@interface PlacesController()

@end

MainModel *main_model;

@implementation PlacesController {
  }

  - ( void )prepareForSegue:( UIStoryboardSegue * )segue sender:( id )sender {
      if ( [segue.identifier isEqualToString:@"embedContainer"] ) {
          _containerViewController = segue.destinationViewController;
      }
  }

  - ( IBAction )click_nearby:( id )sender {

      _btn_near_by.selected = !_btn_near_by.selected;

      if ( _btn_near_by.selected ) {

          self.isNearby = YES;

      } else {

          self.isNearby      = NO;
          self.isSelectedAll = YES;

      }

  }

  - ( void )noLocations; {

      _view_placesView.cnt_container_view.hidden = YES;
      _view_placesView.lbl_message.hidden        = NO;
      _btn_map.enabled                           = NO;
      _btn_list.enabled                          = NO;
      _btn_filters.enabled                       = NO;

  }

  - ( void )hasLocations; {

      _view_placesView.cnt_container_view.hidden = NO;
      _view_placesView.lbl_message.hidden        = YES;
      _btn_map.enabled                           = YES;
      _btn_list.enabled                          = YES;
      _btn_filters.enabled                       = YES;

  }

  - ( IBAction)action_open_map:( id )sender {
      _btn_list.selected = NO;
      _btn_map.selected  = YES;
      [_containerViewController segueTo:_containerViewController.segues[ 0 ]];

  }

  - ( IBAction)action_open_list:( id )sender {
      _btn_list.selected = YES;
      _btn_map.selected  = NO;
      [_containerViewController segueTo:_containerViewController.segues[ 1 ]];
  }

  - ( void )viewWillAppear:( BOOL )animated; {

       }

  - ( void )viewDidDisappear:( BOOL )animated; {
      [super viewDidDisappear:animated];

  }

  - ( void )viewDidLoad {
      [super viewDidLoad];

      _btn_list.selected = YES;
      _btn_map.selected  = NO;

  }

@end
