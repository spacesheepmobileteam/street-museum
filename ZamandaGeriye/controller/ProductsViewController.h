//
//  ProductsViewController.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 29/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductsViewController : UIViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>

    @property(weak, nonatomic) IBOutlet UIButton    *btn_restore;
    @property(weak, nonatomic) IBOutlet UILabel     *lbl_blank_message;
    @property(weak, nonatomic) IBOutlet UITableView *tbl_search;
    @property(weak, nonatomic) IBOutlet UITextField *txt_search;

    @property(nonatomic, copy) NSString *searchParameter;
    @property(nonatomic, strong) id     selected_data;
@end
