//
// Created by ali kiran on 29/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "PlacesMapView.h"
#import "PlacesMapViewDelegate.h"

@protocol PlacesMapDelegate;

@interface PlacesMapController : UIViewController <PlacesMapViewDelegate>

	@property(strong, nonatomic) IBOutlet PlacesMapView *places_map_view;
	@property(nonatomic, weak) id <PlacesMapDelegate>   delegate;
@end
