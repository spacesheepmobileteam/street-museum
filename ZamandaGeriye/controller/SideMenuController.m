//
// Created by ali kiran on 19/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import "SideMenuController.h"
#import "LeftMenuController.h"
#import "UIViewController+JASidePanel.h"

SideMenuController *_self;

@implementation SideMenuController {

	}

	+ ( SideMenuController * )instance {
		static SideMenuController *_instance = nil;

		@synchronized ( self ) {
			if ( _instance == nil) {
				_instance = _self;
			}
		}

		return _instance;
	}

	- ( void )awakeFromNib; {
		[super awakeFromNib];

		_self = self;

		NSLog(@"");

//		[self setRightPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"rightViewController"]];



	}

	- ( void )viewDidLoad; {
		[super viewDidLoad];

		self.panningLimitedToTopViewController = NO;
		UIViewController *panel = [self.storyboard instantiateViewControllerWithIdentifier:@"story_home_nav"];
		[self setCenterPanel:panel];

		LeftMenuController *left_panel = [self.storyboard instantiateViewControllerWithIdentifier:@"story_left_controller"];
		[self setLeftPanel:left_panel];

		left_panel.segue_map[ @"segue_to_home" ] = panel;

		left_panel.sidePanelController.leftFixedWidth                    = 163;
		left_panel.sidePanelController.bounceOnSidePanelClose            = NO;
		left_panel.sidePanelController.bounceOnSidePanelOpen             = NO;
		left_panel.sidePanelController.allowRightOverpan                 = NO ;
		left_panel.sidePanelController.panningLimitedToTopViewController = NO;
		left_panel.sidePanelController.bounceOnCenterPanelChange         = NO;

		if ( [left_panel.sidePanelController respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)] ) {
			left_panel.sidePanelController.automaticallyAdjustsScrollViewInsets = NO;
		}

//		[self showLeftPanelAnimated:NO];
	}

	+ ( UIImage * )defaultImage; {
		return [UIImage imageNamed:@"menu_btn"];
	}

@end
