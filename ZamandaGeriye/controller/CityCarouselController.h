//
//  CityCarouselController.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 12/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityCarouselViewDelegate.h"
#import "CityCarouselView.h"


@interface CityCarouselController : UIViewController<CityCarouselViewDelegate, NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) IBOutlet CityCarouselView *city_carousel_view;

@end
