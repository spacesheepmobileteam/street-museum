//
// Created by ali kiran on 19/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import "LeftMenuController.h"
#import "SideMenuController.h"
#import "UIViewController+JASidePanel.h"
#import "UIViewController+SideMenuHelpers.h"
#import "RootViewController.h"
#import "LandingPageController.h"
#import "MainModel.h"
#import "SelectedFilter.h"
#import "PlacesController.h"
#import "ProductsViewController.h"

MainModel      *main_model;
SelectedFilter *selectedFilter;

@interface LeftMenuController()

	@property(strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

@end

@implementation LeftMenuController {

		NSMutableDictionary *_segue_map;
		UIButton            *selected_button;
	}

	- ( IBAction)menu_button_clicked:( id )sender {

		if ( selected_button != sender ) {
			selected_button.selected = NO;
			selected_button = sender;
			selected_button.selected = YES;
		}
	}

	- ( void )awakeFromNib; {
		[super awakeFromNib];

		_segue_map = [NSMutableDictionary new];
		_txt_search.delegate = self;

	}

	- ( BOOL )textFieldShouldReturn:( UITextField * )textField; {

		if ( textField.text.length ) {

			[selectedFilter clearAllItems];
			selectedFilter.isSearchParameter = YES;
			selectedFilter.searchParameter   = textField.text;

			[self performSegueWithIdentifier:@"segue_to_search" sender:self];

			/*[self menu_button_clicked:(UIButton *) _btn_maps];
			*/

		}

		[textField resignFirstResponder];
		return YES;
	}

	- ( void )viewDidLoad; {
		[super viewDidLoad];

		SideMenuController *side_menu  = self.side_menu;
		RootViewController *controller = (RootViewController *) side_menu.centerPanel;

		NSIndexSet *selected = [self.buttons indexesOfObjectsPassingTest:^BOOL (UIButton *obj, NSUInteger idx, BOOL *stop) {
			return obj.tag == controller.button_index.unsignedIntegerValue;
		}];

		selected_button = self.buttons[ selected.firstIndex ];
		selected_button.selected = YES;

	}

	- ( void )prepareForSegue:( UIStoryboardSegue * )segue sender:( id )sender; {
		[super prepareForSegue:segue sender:sender];

		SideMenuController *side_menu = self.side_menu;

		UINavigationController *nav_cont = (UINavigationController *) side_menu.centerPanel;

		if ( !_segue_map[ segue.identifier ] ) {
			_segue_map[ segue.identifier ] = segue.destinationViewController;
		}

		/*if ( [segue.identifier isEqualToString:@"segue_to_info"] ) {

			LandingPageController *landingPageController = _segue_map[ segue.identifier ];

			[side_menu presentViewController:_segue_map[ segue.identifier ] animated:YES completion:^{

			}];
		} else*/

		if ( [segue.identifier isEqualToString:@"segue_to_info"] ) {



		} else {
			[side_menu setCenterPanel:_segue_map[ segue.identifier ]];
			[side_menu stylePanel:self.view];
		}

		if ( [_segue_map[ segue.identifier ] isKindOfClass:[ProductsViewController class]] ) {

			ProductsViewController *searchResultController = _segue_map[ segue.identifier ];
			searchResultController.searchParameter = _txt_search.text;
		}


/*
		if ( [segue.identifier isEqualToString:@"segue_to_home"] ) {

		} else if ( [segue.identifier isEqualToString:@"segue_to_news"] ) {

		}*/

		NSLog(@"");
	}

@end
