//
// Created by ali kiran on 19/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface LeftMenuController : UIViewController <UITextFieldDelegate>

	@property (weak, nonatomic) IBOutlet UITextField *txt_search;
	@property(nonatomic, strong) NSMutableDictionary *segue_map;
@end
