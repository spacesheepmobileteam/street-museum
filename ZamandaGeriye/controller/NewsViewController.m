//
//  NewsViewController.m
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsView.h"
#import "MainModel.h"
#import "NewsDetailController.h"
#import "News.h"
#import "News+Server.h"
#import "NewsViewCell.h"

MainModel *main_model;

@interface NewsViewController()

@end

@implementation NewsViewController {
      NewsDetailController       *destViewController;
      NSFetchedResultsController *_news_results;
  }

  + ( NewsViewController * )instance {
      static NewsViewController *_instance = nil;

      @synchronized ( self ) {
          if ( _instance == nil) {
              _instance = [[self alloc] init];
          }
      }

      return _instance;
  }

  - ( void )viewDidLoad; {
      [super viewDidLoad];

      _news_results = [News newsWithPredicate:nil
                            target:self resultBlock:^(NSArray *records, id pageManager, BOOL *requestNextPage) {
                  NSLog(@"");
              } failureBlock:^(NSError *error) {

              }];

      [_tbl_news reloadData];
  }

  - ( void )prepareForSegue:( UIStoryboardSegue * )segue sender:( id )sender {
      if ( [segue.identifier isEqualToString:@"sendNewsData"] ) {
          NSIndexPath *indexPath = [_tbl_news indexPathForSelectedRow];
          destViewController = segue.destinationViewController;
          destViewController.select_data = (News *) [_news_results objectAtIndexPath:indexPath];
      }
  }

  - ( void )controller:( NSFetchedResultsController * )controller
      didChangeSection:( id <NSFetchedResultsSectionInfo> )sectionInfo
               atIndex:( NSUInteger )sectionIndex
         forChangeType:( NSFetchedResultsChangeType )type; {

  }

  - ( void )controllerDidChangeContent:( NSFetchedResultsController * )controller; {
      NSLog(@"");

      if ( controller.managedObjectContext.hasChanges ) {
          [_tbl_news reloadData];
      }
  }

  - ( void )controller:( NSFetchedResultsController * )controller
       didChangeObject:( id )anObject
           atIndexPath:( NSIndexPath * )indexPath
         forChangeType:( NSFetchedResultsChangeType )type
          newIndexPath:( NSIndexPath * )newIndexPath; {
      NSLog(@"");
  }

  - ( NSInteger )tableView:( UITableView * )tableView numberOfRowsInSection:( NSInteger )section; {
      id <NSFetchedResultsSectionInfo> sectionInfo = [_news_results sections][ section ];
      return sectionInfo.numberOfObjects;
  }

  - ( UITableViewCell * )tableView:( UITableView * )tableView cellForRowAtIndexPath:( NSIndexPath * )indexPath {

      NewsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NewsViewCell identifier]
                                      forIndexPath:indexPath];
      [self configureCell:cell atIndexPath:indexPath];
      return cell;
  }

  - ( void )configureCell:( UITableViewCell * )cell atIndexPath:( NSIndexPath * )indexPath {
      NewsViewCell *news_cell    = (NewsViewCell *) cell;
      News         *locationType = [_news_results objectAtIndexPath:indexPath];
      news_cell.selected_data = locationType;

  }

@end
