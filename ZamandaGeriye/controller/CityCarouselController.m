//
//  CityCarouselController.m
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 12/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "CityCarouselController.h"
#import "CityCarouselView.h"
#import "MainModel.h"
#import "City.h"
#import "City+Server.h"
#import "InAppPurchaseManager.h"
#import "ProductsViewController.h"
#import "Product.h"

@interface CityCarouselController()

@end

MainModel *main_model;

@implementation CityCarouselController {
      NSFetchedResultsController *_city_result;
  }

  - ( NSInteger )getCityCount:( UIView * )view; {
      id <NSFetchedResultsSectionInfo> sectionInfo = [_city_result sections][ 0 ];
      return [sectionInfo numberOfObjects];
  }

  - ( City * )getCityItem:( int )path at:( UIView * )at; {
      City *city = [_city_result objectAtIndexPath:[NSIndexPath indexPathForRow:path inSection:0]];
      return city;
  }

  - ( void )action_view_item_at:( UIView * )view; {

  }

  - ( void )action_buy_item_at:( UIView * )view; {
      ProductsViewController *product_controller = [self.storyboard instantiateViewControllerWithIdentifier:@"story_product"];
      product_controller.selected_data = [view performSelector:NSSelectorFromString(@"selected_data")];
      [self presentViewController:product_controller animated:YES
                                                     completion:^{

                                                     }];
  }

  - ( BOOL )hasPurchased:( City * )city; {
      __block BOOL has_purchased = NO;
      [city.products enumerateObjectsUsingBlock:^(Product *obj, BOOL *stop) {
          if ( obj.transactions.count > 0 ) {
              has_purchased = YES;
              *stop = YES;
          }

      }];
      return has_purchased;
  }

  - ( void )controller:( NSFetchedResultsController * )controller
       didChangeObject:( id )anObject
           atIndexPath:( NSIndexPath * )indexPath
         forChangeType:( NSFetchedResultsChangeType )type
          newIndexPath:( NSIndexPath * )newIndexPath; {
      NSLog(@"");

  }

  - ( void )controller:( NSFetchedResultsController * )controller
      didChangeSection:( id <NSFetchedResultsSectionInfo> )sectionInfo
               atIndex:( NSUInteger )sectionIndex
         forChangeType:( NSFetchedResultsChangeType )type; {
      NSLog(@"");

  }

  - ( void )controllerDidChangeContent:( NSFetchedResultsController * )controller; {
      NSLog(@"");
      if ( controller.managedObjectContext.hasChanges ) {
          [_city_carousel_view reload];

      }

  }

  - ( void )viewDidLoad {
      [super viewDidLoad];

      _city_carousel_view.cityCarouselViewDelegate = self;

      _city_result = [City citiesWithPredicate:nil target:self
                                                   resultBlock:^(NSArray *records, id pageManager, BOOL *requestNextPage) {
                                                       [[InAppPurchaseManager instance] loadProducts];

                                                   } failureBlock:^(NSError *error) {
                  [[InAppPurchaseManager instance] loadProducts];

              }];

      [_city_carousel_view reload];



  }

@end
