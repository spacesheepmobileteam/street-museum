//
// Created by ali kiran on 19/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JASidePanelController.h"

@interface SideMenuController : JASidePanelController

	+ ( SideMenuController * )instance;

@end
