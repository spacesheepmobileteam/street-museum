//
// Created by ali kiran on 29/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "PlacesListView.h"
#import "PlacesListViewDelegate.h"

@interface PlacesListController : UIViewController <PlacesListViewDelegate>

	@property(strong, nonatomic) IBOutlet PlacesListView *places_list_view;
	@property(nonatomic) double                          selectedNewsId;
	@property(nonatomic) BOOL                            selectedLocation;
	@property(nonatomic) BOOL                            isBackFromDetail;
@end
