#import "ContainerViewController.h"
#import "MainModel.h"

@interface ContainerViewController()

	@property(strong, nonatomic) NSString *currentSegueIdentifier;
@end

MainModel *main_model;

@implementation ContainerViewController {
		NSMutableArray      *_segues;
		NSMutableDictionary *_segue_map;
		id                  notif_call_PlacesListController;
	}

	- ( void )viewDidLoad {
		[super viewDidLoad];
		[self segueTo:_segues[ 1 ]];
	}

	- ( void )prepareForSegue:( UIStoryboardSegue * )segue sender:( id )sender {

		if ( !_segue_map ) {
			_segue_map = [NSMutableDictionary new];
		}

		if ( [_currentSegueIdentifier isEqualToString:segue.identifier] ) {
			return;
		}

		if ( !_segue_map[ segue.identifier ] ) {
			_segue_map[ segue.identifier ] = segue.destinationViewController;
		}

		if ( self.childViewControllers.count > 0 ) {
			UIViewController *previous = _segue_map[ _currentSegueIdentifier ];
			UIViewController *next     = _segue_map[ segue.identifier ];

			next.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

			[self addChildViewController:next];
			[previous willMoveToParentViewController:nil];

			[self transitionFromViewController:previous toViewController:next duration:.3
									   options:UIViewAnimationOptionTransitionCrossDissolve animations:nil
									completion:^(BOOL finished) {

										[previous removeFromParentViewController];
										[next didMoveToParentViewController:self];

									}];
		}
		else {
			[self addChildViewController:segue.destinationViewController];
			( (UIViewController *) segue.destinationViewController ).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

			[self.view addSubview:( (UIViewController *) segue.destinationViewController ).view];
			[segue.destinationViewController didMoveToParentViewController:self];

		}

		_currentSegueIdentifier = segue.identifier;

	}

	- ( void )setValue:( id )value forUndefinedKey:( NSString * )key; {

		if ( [value isKindOfClass:[NSString class]] && [( (NSString *) value ) isEqualToString:@"segue"] ) {
			if ( !_segues ) {
				_segues = [NSMutableArray new];
			}

			[_segues addObject:key];
		} else {
			[super setValue:value forUndefinedKey:key];
		}
	}

	- ( void )viewWillAppear:( BOOL )animated; {
		[super viewWillAppear:animated];

	}

	- ( void )segueTo:( NSString * )name; {
		[self performSegueWithIdentifier:name sender:nil];
	}

@end
