//
//  FilterViewController.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 11/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "MainModel.h"
#import "LocationType.h"
#import "LocationType+Server.h"
#import "FilterViewController.h"
#import "FilterViewCell.h"
#import "UIColor+Serialization.h"

@interface FilterViewController()

    @property(weak, nonatomic) IBOutlet UITableView *tableView;

@end

MainModel *main_model;

@implementation FilterViewController {
      NSFetchedResultsController *_location_results;
  }

  - ( void )viewWillAppear:( BOOL )animated; {

  }

  - ( void )viewDidLoad; {
      [super viewDidLoad];

      _location_results = [LocationType locationTypeWithPredicate:nil
                                        target:self
                                        resultBlock:^(NSArray *records, id pageManager, BOOL *requestNextPage
                                        ) {

                                        } failureBlock:^(NSError *error) {

              }];
  }

  - ( IBAction)click_filter_btn:( id )sender {

  }

  - ( IBAction)click_clear_button:( id )sender {
  }

  - ( IBAction)click_all_btn:( id )sender {

  }

  - ( IBAction)click_near_by_btn:( id )sender {

  }

  - ( void )awakeFromNib {
      [super awakeFromNib];
  }

  - ( void )didReceiveMemoryWarning {
      [super didReceiveMemoryWarning];
      // Dispose of any resources that can be recreated.
  }

  - ( NSInteger )numberOfSectionsInTableView:( UITableView * )tableView {
      return [[_location_results sections] count];
  }

  - ( NSInteger )tableView:( UITableView * )tableView numberOfRowsInSection:( NSInteger )section {
      id <NSFetchedResultsSectionInfo> sectionInfo = [_location_results sections][ section ];
      return [sectionInfo numberOfObjects];
  }

  - ( UITableViewCell * )tableView:( UITableView * )tableView cellForRowAtIndexPath:( NSIndexPath * )indexPath {

      FilterViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[FilterViewCell reuseIdentifier]
                                         forIndexPath:indexPath];
      [self configureCell:cell atIndexPath:indexPath];
      return cell;
  }


  - ( void )configureCell:( UITableViewCell * )cell atIndexPath:( NSIndexPath * )indexPath {
      FilterViewCell *filter_cell  = (FilterViewCell *) cell;
      LocationType    *locationType = [_location_results objectAtIndexPath:indexPath];
      filter_cell.location_type = locationType;

      if ( [indexPath row] % 2 ) {
          [cell setBackgroundColor:[UIColor colorFromHexString:@"424141"]];
      } else {
          [cell setBackgroundColor:[UIColor colorFromHexString:@"4f4e4e"]];
      }
  }

  - ( BOOL )tableView:( UITableView * )tableView canEditRowAtIndexPath:( NSIndexPath * )indexPath {
      // Return NO if you do not want the specified item to be editable.
      return NO;
  }

  - ( void )tableView:( UITableView * )tableView
   commitEditingStyle:( UITableViewCellEditingStyle )editingStyle
    forRowAtIndexPath:( NSIndexPath * )indexPath {
      if ( editingStyle == UITableViewCellEditingStyleDelete ) {
          NSManagedObjectContext *context = [_location_results managedObjectContext];
          [context deleteObject:[_location_results objectAtIndexPath:indexPath]];

          NSError *error = nil;
          if ( ![context save:&error] ) {
              // Replace this implementation with code to handle the error appropriately.
              // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
              NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
              abort();
          }
      }
  }

  - ( BOOL )tableView:( UITableView * )tableView canMoveRowAtIndexPath:( NSIndexPath * )indexPath {
      // The table view should not be re-orderable.
      return NO;
  }

  - ( void )tableView:( UITableView * )tableView didSelectRowAtIndexPath:( NSIndexPath * )indexPath {
      if ( [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad ) {
          NSManagedObject *object = [_location_results objectAtIndexPath:indexPath];
      }
  }

  - ( void )prepareForSegue:( UIStoryboardSegue * )segue sender:( id )sender {
      if ( [[segue identifier] isEqualToString:@"showDetail"] ) {
          NSIndexPath     *indexPath = [self.tableView indexPathForSelectedRow];
          NSManagedObject *object    = [_location_results objectAtIndexPath:indexPath];
      }
  }

  /*- ( void )controllerWillChangeContent:( NSFetchedResultsController * )controller {
      [self.tableView beginUpdates];
  }

  - ( void )controller:( NSFetchedResultsController * )controller
      didChangeSection:( id <NSFetchedResultsSectionInfo> )sectionInfo
               atIndex:( NSUInteger )sectionIndex
         forChangeType:( NSFetchedResultsChangeType )type {
      switch ( type ) {
          case NSFetchedResultsChangeInsert:
              [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                              withRowAnimation:UITableViewRowAnimationFade];
              break;

          case NSFetchedResultsChangeDelete:
              [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                              withRowAnimation:UITableViewRowAnimationFade];
              break;
      }
  }

  - ( void )controller:( NSFetchedResultsController * )controller
       didChangeObject:( id )anObject
           atIndexPath:( NSIndexPath * )indexPath
         forChangeType:( NSFetchedResultsChangeType )type
          newIndexPath:( NSIndexPath * )newIndexPath {
      UITableView *tableView = self.tableView;

      switch ( type ) {
          case NSFetchedResultsChangeInsert:
              [tableView insertRowsAtIndexPaths:@[ newIndexPath ] withRowAnimation:UITableViewRowAnimationFade];
              break;

          case NSFetchedResultsChangeDelete:
              [tableView deleteRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationFade];
              break;

          case NSFetchedResultsChangeUpdate:
              [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
              break;

          case NSFetchedResultsChangeMove:
              [tableView deleteRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationFade];
              [tableView insertRowsAtIndexPaths:@[ newIndexPath ] withRowAnimation:UITableViewRowAnimationFade];
              break;
      }
  }

  - ( void )controllerDidChangeContent:( NSFetchedResultsController * )controller {
      [self.tableView endUpdates];
  }
*/
// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.

 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // In the simplest, most efficient, case, reload the table view.
    [self.tableView reloadData];
}



@end
