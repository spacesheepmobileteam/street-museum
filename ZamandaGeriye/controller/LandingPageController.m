//
//  LandingPageController.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 06/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "LandingPageController.h"
#import "StreetMuseumAppDelegate.h"

#import "Language+Server.h"

MainModel *main_model;

@interface LandingPageController()

    @property(weak, nonatomic) IBOutlet UIActivityIndicatorView *act_progress;

@end

@implementation LandingPageController {
      BOOL                       isModal;
      JLActionSheet              *_language_sheet;
      id                         _incremental_store_listen_id;
      NSFetchedResultsController *_languages;
  }

  - ( UIStatusBarStyle )preferredStatusBarStyle; {
      return UIStatusBarStyleLightContent;
  }

  - ( UIStatusBarAnimation )preferredStatusBarUpdateAnimation; {
      return UIStatusBarAnimationSlide;
  }

  - ( IBAction)click_close:( id )sender {
      [self dismissViewControllerAnimated:YES completion:^{

      }];
  }

  - ( IBAction)click_go:( id )sender {

      id vc = [self.storyboard instantiateViewControllerWithIdentifier:@"story_root"];
      [self.view.window setRootViewController:vc];
  }

  - ( IBAction)click_facebook:( id )sender {

      [[UIApplication sharedApplication]
                      openURL:[NSURL URLWithString:NSLocalizedString(@"https://www.facebook.com/pages/Street-Museum-%C4%B0stanbul/1416861718549426", @"https://www.facebook.com/pages/Street-Museum-%C4%B0stanbul/1416861718549426")]];
  }

  - ( IBAction)click_twitter:( id )sender {

      [[UIApplication sharedApplication]
                      openURL:[NSURL URLWithString:NSLocalizedString(@"https://twitter.com/streetmuseumist", @"https://twitter.com/streetmuseumist")]];
  }

  - ( IBAction)click_blog:( id )sender {
      [[UIApplication sharedApplication]
                      openURL:[NSURL URLWithString:NSLocalizedString(@"http://streetmuseumistanbul.com/", @"http://streetmuseumistanbul.com/")]];
  }

  - ( IBAction)click_instagram:( id )sender {

      [[UIApplication sharedApplication]
                      openURL:[NSURL URLWithString:NSLocalizedString(@"http://streetmuseumistanbul.com/", @"http://streetmuseumistanbul.com/")]];
  }

  - ( void )viewWillAppear:( BOOL )animated; {
      NSLog(@"");

      _languages = [Language languagesWithPredicate:nil target:self
                                                        resultBlock:^(NSArray *records, id pageManager, BOOL *requestNextPage) {
                                                            NSLog(@"");
                                                            [_act_progress stopAnimating];
                                                        } failureBlock:^(NSError *error) {
                  NSLog(@"");
              }];

      isModal = self.isBeingPresented;

      if ( self.isBeingPresented ) {
          _btn_close.hidden = NO;
      } else {
          _btn_close.hidden = YES;
      }

      _btn_go.
              hidden = YES;

  }

  - ( void )controllerDidChangeContent:( NSFetchedResultsController * )controller; {
      NSLog(@"");
  }

  - ( void )viewDidDisappear:( BOOL )animated; {
      [super viewDidDisappear:animated];

      [[NSNotificationCenter defaultCenter] removeObserver:_incremental_store_listen_id];
  }

  - ( IBAction)open_language_picker:( id )sender {

      NSMutableArray *buttonTitles = [NSMutableArray new];

      [_languages.fetchedObjects enumerateObjectsUsingBlock:^(Language *obj, NSUInteger idx, BOOL *stop) {
          [buttonTitles addObject:obj.title];
      }];

      NSString *sheetTitle = NSLocalizedString(@"Select your language", @"Select your language");

      _language_sheet = [JLActionSheet sheetWithTitle:sheetTitle delegate:self cancelButtonTitle:nil
                                                                 otherButtonTitles:buttonTitles];
      [_language_sheet allowTapToDismiss:YES];
      [_language_sheet setStyle:JLSTYLE_SUPERCLEAN];
      [_language_sheet showOnViewController:self];

  }

  - ( void )actionSheet:( JLActionSheet * )actionSheet clickedButtonAtIndex:( NSInteger )buttonIndex; {

  }

  - ( void )actionSheet:( JLActionSheet * )actionSheet didDismissButtonAtIndex:( NSInteger )buttonIndex; {
      NSLog(@"");

      id <NSFetchedResultsSectionInfo> sectionInfo = [_languages sections][ 0 ];
      Language                         *language   = [_languages objectAtIndexPath:[NSIndexPath indexPathForRow:sectionInfo.numberOfObjects - buttonIndex - 1
                                                                                                inSection:0]];
      main_model.language = language;

      _btn_language.titleLabel.text = main_model.language.title;
      _btn_go.hidden                = isModal;
  }

  - ( void )actionSheetCancel:( UIActionSheet * )actionSheet; {
  }

  - ( void )viewDidLoad {
      [super viewDidLoad];

  }

@end
