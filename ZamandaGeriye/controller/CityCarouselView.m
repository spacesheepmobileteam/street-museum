//
//  CityCarouselView.m
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 12/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <objc/runtime.h>
#import <BlocksKit/UIControl+BlocksKit.h>
#import <BlocksKit/NSObject+BlocksKit.h>
#import "CityCarouselView.h"
#import "City.h"
#import "CityCarouselViewDelegate.h"
#import "CityHorizontalView.h"
#import "CityVerticalView.h"
#import "UIImageView+AFRequest.h"
#import "NSString+ContainsString.h"
#import "SideMenuController.h"
#import "Product.h"

@interface CityCarouselView()

    @property(nonatomic, strong) NSMutableArray *items;

@end

@implementation CityCarouselView {
      VIEWING_OPTION_SEGMENT *segment;
      BOOL                   _wrap;

  }

  - ( void )awakeFromNib; {

      _btn_list.selected     = NO;
      _btn_carousel.selected = YES;

      [_btn_carousel addEventHandler:^(id sender) {
          _btn_list.selected     = NO;
          _btn_carousel.selected = YES;

          [self setCarouselType:VIEWING_SEGMENT_CAROUSEL];

      } forControlEvents:UIControlEventTouchUpInside];

      [_btn_list addEventHandler:^(id sender) {
          _btn_list.selected     = YES;
          _btn_carousel.selected = NO;
          [self setCarouselType:VIEWING_SEGMENT_LIST];

      } forControlEvents:UIControlEventTouchUpInside];

      self.items = [NSMutableArray array];

      segment = (VIEWING_OPTION_SEGMENT *) VIEWING_SEGMENT_LIST;

      _carousel.vertical               = YES;
      _carousel.scrollToItemBoundary   = NO;
      _carousel.centerItemWhenSelected = NO;
      _wrap = NO;

      if ( !IS_IPHONE_5) {
          _carousel.contentOffset = CGSizeMake(0, -80);
      } else {
          _carousel.contentOffset = CGSizeMake(0, -110);
      }

      _carousel.bounceDistance = 0.2;

      [_carousel reloadData];

  }

  - ( void )setCarouselType:( VIEWING_OPTION_SEGMENT )_segment {

      segment = (VIEWING_OPTION_SEGMENT *) _segment;

      if ( segment == (VIEWING_OPTION_SEGMENT *) VIEWING_SEGMENT_CAROUSEL ) {

          _carousel.vertical               = NO;
          _carousel.scrollToItemBoundary   = YES;
          _carousel.centerItemWhenSelected = YES;
          _carousel.contentOffset          = CGSizeZero;
          _carousel.decelerationRate       = 0.98f;
          _wrap = NO;

//			_cons_vertical.constant = 0;

      } else {
          _carousel.vertical               = YES;
          _carousel.scrollToItemBoundary   = NO;
          _carousel.centerItemWhenSelected = NO;
          _carousel.decelerationRate       = 0.98f;

//			_cons_vertical.constant = -200;

          if ( !IS_IPHONE_5) {
              _carousel.contentOffset = CGSizeMake(0, -80);
          } else {
              _carousel.contentOffset = CGSizeMake(0, -110);
          }

          _wrap = NO;

      }

      [_carousel reloadData];
      //[self bringSubviewToFront:_carousel];

      [_carousel scrollToItemAtIndex:0 animated:YES];

  }

  - ( UIView * )hitTest:( CGPoint )point withEvent:( UIEvent * )event {
      UIView *hitView = [super hitTest:point withEvent:event];

      NSString *class_name = [NSString stringWithUTF8String:class_getName([hitView class])];

      if ( [class_name containsString:@"CityHorizontalView"] ) {

          [SideMenuController instance].recognizesPanGesture = NO;

          [self performBlock:^(id sender) {
              [SideMenuController instance].recognizesPanGesture = YES;

          } afterDelay:1.f];

      } else {
          [SideMenuController instance].recognizesPanGesture = YES;
      }

      return hitView;
  }

  - ( CGFloat )carousel:( iCarousel * )carousel valueForOption:( iCarouselOption )option withDefault:( CGFloat )value {

      switch ( option ) {
          case iCarouselOptionWrap: {
              return _wrap;
          }

          case iCarouselOptionArc: {
              if ( segment == (VIEWING_OPTION_SEGMENT *) VIEWING_SEGMENT_CAROUSEL ) {
                  return 2 * M_PI * 0.20f;
              } else {
                  return 2 * M_PI * 0.02f;
              }
          }
          case iCarouselOptionRadius: {
              if ( segment == (VIEWING_OPTION_SEGMENT *) VIEWING_SEGMENT_CAROUSEL ) {
                  return value * 0.62f;
              } else {
                  return value * 0.62f;
              }
          }

          case iCarouselOptionSpacing: {
              if ( segment == (VIEWING_OPTION_SEGMENT *) VIEWING_SEGMENT_CAROUSEL ) {
                  return 1.8f;
              } else {
                  return 1.1f;
              }
          }
          case iCarouselOptionVisibleItems: {
              if ( segment == (VIEWING_OPTION_SEGMENT *) VIEWING_SEGMENT_CAROUSEL ) {
                  return 5;
              } else {
                  return 5;
              }
          }
          default: {
              if ( segment == (VIEWING_OPTION_SEGMENT *) VIEWING_SEGMENT_CAROUSEL ) {
                  return value;
              } else {
                  return value;
              }
          }
      }
  }

  - ( NSUInteger )numberOfItemsInCarousel:( iCarousel * )carousel {

      return (NSUInteger) [_cityCarouselViewDelegate getCityCount:self];
  }

  - ( void )carouselDidScroll:( iCarousel * )carousel {
      // NSLog(@"%f %@", carousel.scrollOffset, NSStringFromCGSize(carousel.contentOffset));
  }

  - ( void )carouselCurrentItemIndexDidChange:( iCarousel * )carousel {
      // NSLog(@"%i", carousel.currentItemIndex);
  }

  - ( UIView * )carousel:( iCarousel * )carousel viewForItemAtIndex:( NSUInteger )index reusingView:( UIView * )view {

      City *city = [_cityCarouselViewDelegate getCityItem:index at:self];

      if ( segment == (VIEWING_OPTION_SEGMENT *) VIEWING_SEGMENT_CAROUSEL ) {

          if ( segment == (VIEWING_OPTION_SEGMENT *) VIEWING_SEGMENT_CAROUSEL ) {

              BOOL b = view && [view isKindOfClass:[CityHorizontalView class]];

//				CityHorizontalView *_horizontalView = (CityHorizontalView *) ( b ? view : [CityHorizontalView new:_cityCarouselViewDelegate] );


              CityHorizontalView *_horizontalView = [[[NSBundle mainBundle]
                                                                loadNibNamed:@"CityHorizontalView" owner:self
                                                                                                   options:nil]
                                                                objectAtIndex:0];

              _horizontalView.delegate      = _cityCarouselViewDelegate;
              _horizontalView.selected_data = city;

              return _horizontalView;
          }

//			}

      } else {

          if ( segment == (VIEWING_OPTION_SEGMENT *) VIEWING_SEGMENT_LIST ) {

              BOOL b = view && [view isKindOfClass:[CityVerticalView class]];

              CityVerticalView *_verticalView = [[[NSBundle mainBundle]
                                                            loadNibNamed:@"CityVerticalView" owner:self options:nil]
                                                            objectAtIndex:0];

              _verticalView.delegate      = _cityCarouselViewDelegate;
              _verticalView.selected_data = city;

              return _verticalView;

          }
      }

      return nil;
  }

  - ( void )reload; {
      [_carousel reloadData];

  }
@end
