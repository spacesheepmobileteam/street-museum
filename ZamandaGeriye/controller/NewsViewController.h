//
//  NewsViewController.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoBrowserDelegate.h"
#import "NewsViewDelegate.h"

@class NewsView;

@interface NewsViewController : UIViewController <PhotoBrowserDelegate, NewsViewDelegate, NSFetchedResultsControllerDelegate>

    @property(weak, nonatomic) IBOutlet UITableView *tbl_news;

    + ( NewsViewController * )instance;

    - ( double )getSelectedNewsId;
@end
