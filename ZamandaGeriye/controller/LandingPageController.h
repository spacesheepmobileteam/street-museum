//
//  LandingPageController.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 06/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JLActionSheet.h"

@interface LandingPageController : UIViewController <JLActionSheetDelegate, NSFetchedResultsControllerDelegate>

    @property(weak, nonatomic) IBOutlet UIButton *btn_language;
    @property(weak, nonatomic) IBOutlet UIButton *btn_go;
    @property(weak, nonatomic) IBOutlet UIButton *btn_close;
    @property(nonatomic, strong) NSNumber        *button_index;

@end
