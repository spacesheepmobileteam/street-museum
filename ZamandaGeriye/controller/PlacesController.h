//
//  PlacesController.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlacesListViewDelegate.h"
#import "PlacesListView.h"

@class ContainerViewController;

@interface PlacesController : UIViewController

    @property(strong, nonatomic) IBOutlet PlacesListView *view_placesView;
    @property(nonatomic, weak) ContainerViewController   *containerViewController;

    @property(weak, nonatomic) IBOutlet UIButton *btn_map;
    @property(weak, nonatomic) IBOutlet UIButton *btn_list;
    @property(weak, nonatomic) IBOutlet UIButton *btn_near_by;
    @property(weak, nonatomic) IBOutlet UIButton *btn_filters;
    @property(weak, nonatomic) IBOutlet UILabel  *lbl_search;

    @property(nonatomic) BOOL isNearby;

    @property(nonatomic) BOOL isSelectedAll;

    @property(nonatomic) BOOL isSearchParameter;

    @property(nonatomic, copy) NSString *searchParameter;

@end
