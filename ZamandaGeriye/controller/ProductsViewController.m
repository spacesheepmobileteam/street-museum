//
//  ProductsViewController.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 29/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "ProductsViewController.h"
#import "Product.h"
#import "Product+Server.h"
#import "ProductViewCell.h"

@interface ProductsViewController()

@end

@implementation ProductsViewController {
      NSFetchedResultsController *_product_results;
  }

  - ( void )performSegueWithIdentifier:( NSString * )identifier sender:( id )sender; {
      [super performSegueWithIdentifier:identifier sender:sender];
  }

  - ( void )viewWillAppear:( BOOL )animated; {
  }

  - ( IBAction)action_editing_change:( UITextField * )sender {
      NSString *search_text = sender.text;

      dispatch_async(dispatch_get_global_queue(0, 0), ^{
          [MagicalRecord saveUsingCurrentThreadContextWithBlockAndWait:^(NSManagedObjectContext *localContext) {

              //@"name CONTAINS[cd] %@ OR desc CONTAINS[cd] %@ "
              NSFetchRequest *predicate = [Product MR_requestAllSortedBy:@"name" ascending:YES
                                                                                 withPredicate:[NSPredicate predicateWithFormat:@"ANY cities.title CONTAINS[cd] %@ OR ANY cities.desc CONTAINS[cd] %@ ",
                                                                                                                                search_text,
                                                                                                                                search_text]
                                                                                 inContext:localContext];
              predicate.returnsDistinctResults = YES;

              NSArray *results = [Product MR_executeFetchRequest:predicate inContext:localContext];

              NSLog(@"%@", results);

          }];

      });
  }

  - ( IBAction)action_search_end:( UITextField * )sender {
      NSLog(@"");

  }

  - ( IBAction)action_restore:( id )sender {
  }

  - ( void )viewDidLoad {
      [super viewDidLoad];

      _product_results = [Product productsWithPredicate:nil
                                  target:self resultBlock:^(NSArray *records, id pageManager, BOOL *requestNextPage) {

              } failureBlock:^(NSError *error) {

              }];
      [_tbl_search reloadData];

  }

  - ( BOOL )textFieldShouldReturn:( UITextField * )textField; {
      [textField resignFirstResponder];
      return YES;
  }

  - ( void )controller:( NSFetchedResultsController * )controller
      didChangeSection:( id <NSFetchedResultsSectionInfo> )sectionInfo
               atIndex:( NSUInteger )sectionIndex
         forChangeType:( NSFetchedResultsChangeType )type; {

  }

  - ( void )controllerDidChangeContent:( NSFetchedResultsController * )controller; {
      NSLog(@"");

      if ( controller.managedObjectContext.hasChanges ) {
          [_tbl_search reloadData];

      }
  }

  - ( void )controller:( NSFetchedResultsController * )controller
       didChangeObject:( id )anObject
           atIndexPath:( NSIndexPath * )indexPath
         forChangeType:( NSFetchedResultsChangeType )type
          newIndexPath:( NSIndexPath * )newIndexPath; {
      NSLog(@"");
  }

  - ( NSInteger )tableView:( UITableView * )tableView numberOfRowsInSection:( NSInteger )section; {
      id <NSFetchedResultsSectionInfo> sectionInfo = [_product_results sections][ section ];
      return sectionInfo.numberOfObjects;
  }

  - ( UITableViewCell * )tableView:( UITableView * )tableView cellForRowAtIndexPath:( NSIndexPath * )indexPath {

      ProductViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ProductViewCell reuseIdentifier]
                                         forIndexPath:indexPath];
      [self configureCell:cell atIndexPath:indexPath];
      return cell;
  }

  - ( void )configureCell:( UITableViewCell * )cell atIndexPath:( NSIndexPath * )indexPath {
      ProductViewCell *filter_cell  = (ProductViewCell *) cell;
      Product         *locationType = [_product_results objectAtIndexPath:indexPath];
      filter_cell.selected_data = locationType;

  }

@end
