//
//  NewsDetailController.m
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "NewsDetailController.h"
#import "NewsDetailView.h"
#import "IDMPhotoBrowser.h"
#import "News.h"
#import "Location.h"

@interface NewsDetailController()

@end

@implementation NewsDetailController

  - ( IBAction)click_like_button:( id )sender {

  }

  - ( NewsDetailView * )detail_view {
      return self.view;
  }

  - ( void )prepareForSegue:( UIStoryboardSegue * )segue sender:( id )sender {

  }


  - ( id )initWithNibName:( NSString * )nibNameOrNil bundle:( NSBundle * )nibBundleOrNil {
      self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
      if ( self ) {

      }
      return self;
  }

  - ( void )setSelect_data:( News * )select_data; {
    /*  _select_data = select_data;
      NewsDetailView *newsDetailView = (NewsDetailView *)self.view;
      newsDetailView.selected_data = select_data;
      NSLog(@"");*/
      _select_data = select_data;
  }

  - ( void )viewDidLoad; {
      [super viewDidLoad];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDataModelChange:)
                                                             name:NSManagedObjectContextObjectsDidChangeNotification
                                                             object:[NSManagedObjectContext MR_defaultContext]];
//      self.detail_view.selected_data = _select_data;

      [_select_data startDetailRequestWithDomain:self resultBlock:^(MMRecord *record) {
          NSLog(@"");
          _select_data = (News *) record;
          self.detail_view.selected_data = _select_data;

      } failureBlock:^(NSError *error) {
          NSLog(@"");
      }];
  }

  - ( void )handleDataModelChange:( NSNotification * )note {
      NSSet *updatedObjects  = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
      NSSet *deletedObjects  = [[note userInfo] objectForKey:NSDeletedObjectsKey];
      NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];

      NSLog(@"");
      // Do something in response to this
  }

  - ( void )openPhotoBrowserWithArray:( NSArray * )_array; {

      IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:_array];
      if ( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
          browser.modalPresentationCapturesStatusBarAppearance = NO;
      }

      [[UIApplication sharedApplication] setStatusBarHidden:NO];
      [self presentViewController:browser animated:NO completion:^{

      }];

  }

@end
