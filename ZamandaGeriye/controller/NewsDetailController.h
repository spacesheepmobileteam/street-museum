//
//  NewsDetailController.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoBrowserDelegate.h"

@class News;

@interface NewsDetailController : UIViewController <PhotoBrowserDelegate>

    @property(nonatomic, strong) News *select_data;
@end
