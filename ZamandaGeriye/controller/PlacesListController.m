//
// Created by ali kiran on 29/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import "PlacesListController.h"
#import "MainModel.h"
#import "NewsDetailController.h"
#import "Location.h"

@implementation PlacesListController {

		id                   notif_model_done;
		NewsDetailController *destViewController;
		id                   notif_call_PlacesMapController;

		id notif_call_no_locations;
	}

	/*- ( void )prepareForSegue:( UIStoryboardSegue * )segue sender:( id )sender {
		if ( [segue.identifier isEqualToString:@"segue_location_detail"] ) {
			NSIndexPath *indexPath = [_places_list_view.tbl_list indexPathForSelectedRow];
			destViewController = segue.destinationViewController;

*//*
			NSArray *locationsArray = main_model.last_locations_Response.grouped_locations[ (NSUInteger) indexPath.section ];

			Locations *locations = locationsArray[ (NSUInteger) indexPath.row ];

			[selectedLocation setLocationItem:locations type:self];

			_selectedNewsId = selectedLocation.self_location.detailId;

			destViewController.newsDetailId     = _selectedNewsId;
			destViewController.selectedItemType = @"locations";*//*
			NSLog(@"");
		}
	}

	- ( void )viewDidLoad; {
		[super viewDidLoad];

		_places_list_view.delegate = self;

	}

	- ( void )viewWillAppear:( BOOL )animated; {

		if ( selectedLocation.isFull ) {
			selectedLocation.isShowDetail = NO;
		}

		notif_model_done = [[NSNotificationCenter defaultCenter]
												  addObserverForName:@"model_done" object:nil queue:nil
														  usingBlock:^(NSNotification *note) {

															  if ( !selectedLocation.isFull ) {
																  [_places_list_view reload];
															  }

														  }];

		notif_call_PlacesMapController = [[NSNotificationCenter defaultCenter]
																addObserverForName:@"refreshPlacesListTable" object:nil
																			 queue:nil
																		usingBlock:^(NSNotification *note) {

																			[_places_list_view.tbl_list deselectRowAtIndexPath:[_places_list_view.tbl_list indexPathForSelectedRow]
																													  animated:YES];

																		}];

		notif_call_PlacesMapController = [[NSNotificationCenter defaultCenter]
																addObserverForName:@"selectedPlacesMapItem" object:nil
																			 queue:nil
																		usingBlock:^(NSNotification *note) {

																			NSArray *locationsArray        = main_model.last_locations_Response.locations;
																			NSArray *locationsGroupedArray = main_model.last_locations_Response.grouped_locations;

																			[locationsGroupedArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
																				NSMutableArray *mutableArray = locationsGroupedArray[ idx ];

																				[mutableArray enumerateObjectsUsingBlock:^(id obj2, NSUInteger idx2, BOOL *stop2) {
																					Locations *locations = mutableArray[ idx2 ];
																					if ( locations.internalBaseClassIdentifier == [(Locations *) note.object internalBaseClassIdentifier] ) {
																						[_places_list_view.tbl_list selectRowAtIndexPath:[NSIndexPath indexPathForRow:idx2
																																							inSection:idx]
																																animated:NO
																														  scrollPosition:UITableViewScrollPositionMiddle];
																						*stop  = YES;
																						*stop2 = YES;

																					}
																				}];

																			}];

																			*//*[main_model.last_locations_Response.locations enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {

																				if ( [(Locations *) note.object internalBaseClassIdentifier] == [(Locations *) locationsArray[ idx ] internalBaseClassIdentifier] ) {

																					[_places_list_view.tbl_list selectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:NAN] animated:NO scrollPosition:UITableViewScrollPositionMiddle];
																					*stop = YES;
																				}

																			}];*//*

																		}];

	}

	- ( void )openLocationOnMapWithCellIndex:( int )index; {

		[selectedLocation setLocationItem:main_model.last_locations_Response.locations[ index ] type:self];

		[[NSNotificationCenter defaultCenter]
							   postNotificationName:@"openMapWithLocationPosition" object:nil];

	}

	- ( NSInteger )getLocationsCount:( PlacesListView * )view; {
		return main_model.last_locations_Response.locations.count;
	}

	- ( NSInteger )getGroupedLocationsCount:( PlacesListView * )view; {
		return main_model.last_locations_Response.grouped_locations.count;
	}

	- ( Location * )getLocationsItem:( int )path at:( PlacesListView * )at; {
		return main_model.last_locations_Response.locations[ path ];
	}

	- ( NSMutableArray * )getGroupedLocationsArray:( int )path at:( PlacesListView * )at; {
		return main_model.last_locations_Response.grouped_locations[ path ];
	}*/
@end
