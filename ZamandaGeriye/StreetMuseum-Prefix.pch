//
//  Prefix header
//
//  The contents of this file are implicitly included at the beginning of every source file.
//






#ifdef __OBJC__

#import <Availability.h>
#import <UIKit/UIKit.h>
#import <mach/mach_time.h>
#import <Foundation/Foundation.h>
#import "CoreData+MagicalRecord.h"

#import "logger.h"

typedef enum {
    VIEWING_SEGMENT_LIST = 0, VIEWING_SEGMENT_CAROUSEL
} VIEWING_OPTION_SEGMENT;

#ifndef __IPHONE_3_0
#warning "This project uses features only available in iOS SDK 3.0 and later."
#endif

#define AppName                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"]
#define IOSVersion            [[UIDevice currentDevice] systemVersion]
#define AppVersion              [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_RETINA ([[UIScreen mainScreen] scale] == 2.0f)

#define SEGUE_LIST @"embedList"
#define SEGUE_MAP @"embedMap"

#define NSLog(__FORMAT__, ...) logger(stderr,("%s%x]>%s:%d %s %s\n"), [NSThread currentThread].isMainThread ? "*" : "",(uint)[NSThread currentThread],((NSString *) [NSString stringWithUTF8String:__FILE__]).lastPathComponent.UTF8String,__LINE__, __PRETTY_FUNCTION__,  [[NSString stringWithFormat:__FORMAT__, ##__VA_ARGS__] UTF8String])

#define StringOrEmpty(A)  ({ __typeof__(A) __a = (A); __a ? __a : @""; })

static int getUptimeInMillis () {
    const int64_t                    kOneMillion = 1000 * 1000;
    static mach_timebase_info_data_t s_timebase_info;

    if ( s_timebase_info.denom == 0 ) {
        (void) mach_timebase_info(&s_timebase_info);
    }

    // mach_absolute_time() returns billionth of seconds,
    // so divide by one million to get milliseconds
    return (int) ( ( mach_absolute_time() * s_timebase_info.numer ) / ( kOneMillion * s_timebase_info.denom ) );
}

static char from_hex (char ch) {
    return isdigit(ch) ? ch - '0' : tolower(ch) - 'a' + 10;
}

static char *url_decode (char *str) {
    char *pstr = str, *buf = malloc(strlen(str) + 1), *pbuf = buf;
    while ( *pstr ) {
        if ( *pstr == '%' ) {
            if ( pstr[ 1 ] && pstr[ 2 ] ) {
                *pbuf++ = from_hex(pstr[ 1 ]) << 4 | from_hex(pstr[ 2 ]);
                pstr += 2;
            }
        } else if ( *pstr == '+' ) {
            *pbuf++ = ' ';
        } else {
            *pbuf++ = *pstr;
        }
        pstr++;
    }

    *pbuf = '\0';
    return buf;
}

static NSString *const RMStoreTransactionsKeychainKey = @"RMStoreTransactions";

#pragma mark - Keychain

static NSMutableDictionary *RMKeychainGetSearchDictionary (NSString * key) {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:(__bridge id) kSecClassGenericPassword forKey:(__bridge id) kSecClass];

    NSData *encodedIdentifier = [key dataUsingEncoding:NSUTF8StringEncoding];

    [dictionary setObject:encodedIdentifier forKey:(__bridge id) kSecAttrGeneric];
    [dictionary setObject:encodedIdentifier forKey:(__bridge id) kSecAttrAccount];

    NSString *serviceName = [NSBundle mainBundle].bundleIdentifier;
    [dictionary setObject:serviceName forKey:(__bridge id) kSecAttrService];

    return dictionary;
}

static void RMKeychainSetValue (NSData * value, NSString * key) {
    NSMutableDictionary *searchDictionary = RMKeychainGetSearchDictionary(key);
    OSStatus            status            = errSecSuccess;
    CFTypeRef           ignore;
    if ( SecItemCopyMatching((__bridge CFDictionaryRef) searchDictionary, &ignore) == errSecSuccess ) { // Update
        if ( !value ) {
            status = SecItemDelete((__bridge CFDictionaryRef) searchDictionary);
        } else {
            NSMutableDictionary *updateDictionary = [NSMutableDictionary dictionary];
            [updateDictionary setObject:value forKey:(__bridge id) kSecValueData];
            status = SecItemUpdate((__bridge CFDictionaryRef) searchDictionary,
                                   (__bridge CFDictionaryRef) updateDictionary);
        }
    } else if ( value ) { // Add
        [searchDictionary setObject:value forKey:(__bridge id) kSecValueData];
        status = SecItemAdd((__bridge CFDictionaryRef) searchDictionary, NULL);
    }
    if ( status != errSecSuccess ) {
        NSLog(@"RMStoreKeychainPersistence: failed to set key %@ with error %ld.", key, status);
    }
}

static NSData *RMKeychainGetValue (NSString * key) {
    NSMutableDictionary *searchDictionary = RMKeychainGetSearchDictionary(key);
    [searchDictionary setObject:(__bridge id) kSecMatchLimitOne forKey:(__bridge id) kSecMatchLimit];
    [searchDictionary setObject:(id) kCFBooleanTrue forKey:(__bridge id) kSecReturnData];

    CFDataRef value  = nil;
    OSStatus  status = SecItemCopyMatching((__bridge CFDictionaryRef) searchDictionary, ( CFTypeRef * ) & value);
    if ( status != errSecSuccess && status != errSecItemNotFound ) {
        NSLog(@"RMStoreKeychainPersistence: failed to get key %@ with error %ld.", key, status);
    }
    return (__bridge NSData *) value;
}

static NS_INLINE void forceImageDecompression (UIImage * image) {
    CGImageRef      imageRef   = [image CGImage];
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef    context    = CGBitmapContextCreate(NULL, CGImageGetWidth(imageRef), CGImageGetHeight(imageRef), 8,
                                                       CGImageGetWidth(imageRef) * 4, colorSpace,
                                                       kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little);
    CGColorSpaceRelease(colorSpace);
    if ( !context ) {
        NSLog(@"Could not create context for image decompression");
        return;
    }
    CGContextDrawImage(context, (CGRect) {
            { 0.0f, 0.0f }, { CGImageGetWidth(imageRef), CGImageGetHeight(imageRef) }
    }, imageRef);
    CFRelease(context);
}

#endif

