//
// Created by Serdar Yıllar on 27/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//



#import "MainModel.h"
#import "LocationType.h"
#import "Language.h"
#import "User.h"

static NSString *const main_model_path = @"MainModel.sqlite";

@implementation MainModel {

      User *_user;
  }

  + ( MainModel * )instance {
      static MainModel *_instance = nil;

      @synchronized ( self ) {
          if ( _instance == nil) {
              _instance = [[self alloc] init];
          }
      }

      return _instance;
  }

  - ( id )init; {
      self = [super init];
      if ( self ) {

          [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:main_model_path];

          NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
          _user = [User MR_findFirst];

          if ( !_user ) {
              [MagicalRecord saveUsingCurrentThreadContextWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                  _user = [User MR_createInContext:localContext];
              }];
          }

      }

      return self;
  }

  - ( void )setLanguage:( Language * )language; {

      [MagicalRecord saveUsingCurrentThreadContextWithBlockAndWait:^(NSManagedObjectContext *localContext) {
          _user.language = language;

          if ( language.identifier.integerValue == 1 ) {
              [[NSUserDefaults standardUserDefaults]
                               setObject:[NSArray arrayWithObjects:@"tr", @"en", @"de", nil] forKey:@"AppleLanguages"];
              [[NSUserDefaults standardUserDefaults] synchronize]; //to make the change immediate
          }

          if ( language.identifier.integerValue == 2 ) {
              [[NSUserDefaults standardUserDefaults]
                               setObject:[NSArray arrayWithObjects:@"en", @"tr", @"de", nil] forKey:@"AppleLanguages"];
              [[NSUserDefaults standardUserDefaults] synchronize]; //to make the change immediate
          }

          if ( language.identifier.integerValue == 3 ) {
              [[NSUserDefaults standardUserDefaults]
                               setObject:[NSArray arrayWithObjects:@"de", @"tr", @"en", nil] forKey:@"AppleLanguages"];
              [[NSUserDefaults standardUserDefaults] synchronize]; //to make the change immediate
          }

          NSLog(@"");
      }];
  }

  - ( Language * )language; {
      return _user.language;
  }

@end
