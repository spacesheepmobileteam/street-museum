//
// Created by ali kiran on 02/01/14.
// Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import <BlocksKit/UIAlertView+BlocksKit.h>
#import "InAppPurchaseManager.h"
#import "Product.h"
#import "Product+Server.h"
#import "RMStoreTransactionReceiptVerificator.h"
#import "RMStoreAppReceiptVerificator.h"
#import "User.h"
#import "MainModel.h"
#import "IAPTransaction.h"
#import "City.h"

@implementation InAppPurchaseManager {

      NSFetchedResultsController           *_product_result;
      NSObject <RMStoreReceiptVerificator> *_receiptVerificator;
      BOOL                                 _appstore_products_loaded;
      NSArray                              *_apple_products;
      NSMutableDictionary                  *_apple_products_by_id;
      BOOL                                 _product_has_loadeded;
  }

  + ( InAppPurchaseManager * )instance {
      static InAppPurchaseManager *_instance = nil;

      @synchronized ( self ) {
          if ( _instance == nil) {
              _instance = [[self alloc] init];

          }
      }

      return _instance;
  }

  - ( id )init; {
      self = [super init];
      if ( self ) {
          const BOOL iOS7OrHigher = floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1;
          _receiptVerificator     = iOS7OrHigher ? [[RMStoreAppReceiptVerificator alloc]
                                                                                  init] : [[RMStoreTransactionReceiptVerificator alloc]
                                                                                  init];
          [RMStore defaultStore].receiptVerificator = _receiptVerificator;

          [RMStore defaultStore].transactionPersistor = self;
      }

      return self;
  }

  - ( void )controllerDidChangeContent:( NSFetchedResultsController * )controller; {
      NSLog(@"");

  }

  - ( void )persistTransaction:( SKPaymentTransaction * )transaction; {

      [MagicalRecord saveUsingCurrentThreadContextWithBlockAndWait:^(NSManagedObjectContext *localContext) {
          Product *product = [Product MR_findFirstByAttribute:@"product_app_store_id"
                                      withValue:transaction.payment.productIdentifier inContext:localContext];
          assert(product);
          IAPTransaction *iapTransaction = [IAPTransaction MR_createInContext:localContext];
          iapTransaction.quantity               = [NSNumber numberWithInteger:transaction.payment.quantity];
          iapTransaction.transaction_date       = transaction.transactionDate;
          iapTransaction.transaction_identifier = transaction.transactionIdentifier;

          iapTransaction.product = product;

          User *user = [[MainModel instance].user MR_inContext:localContext];
          assert(user);
          iapTransaction.user = user;

      }];

      NSLog(@"transaction saved");

  }

  - ( void )loadProducts; {

      _product_has_loadeded = YES;

      _appstore_products_loaded = NO;

      _product_result = [Product productsWithPredicate:nil
                                 target:self resultBlock:^(NSArray *records, id pageManager, BOOL *requestNextPage) {
                  [self loadAppstoreProducts];

              } failureBlock:^(NSError *error) {

              }];

  }

  - ( void )loadAppstoreProducts; {
      NSLog(@"");
      [[RMStore defaultStore]
                requestProducts:[[NSSet alloc] initWithObjects:@"packet.avrupa", @"guide.istanbul", @"guide.izmir", nil]
                success:^(NSArray *products, NSArray *invalidProductIdentifiers) {
                    dispatch_async(dispatch_get_global_queue(0, 0), ^{
                        @autoreleasepool {

                            _apple_products       = products;
                            _apple_products_by_id = [[NSMutableDictionary alloc] initWithCapacity:products.count];
                            [MagicalRecord saveUsingCurrentThreadContextWithBlockAndWait:^(NSManagedObjectContext *localContext) {

                                NSArray *cities = [City MR_findAllInContext:localContext];

                                [_apple_products enumerateObjectsUsingBlock:^(SKProduct *obj, NSUInteger idx, BOOL *stop) {
                                    Product *product = [Product MR_findFirstByAttribute:@"product_app_store_id"
                                                                withValue:obj.productIdentifier inContext:localContext];

                                    if ( !product ) {
                                        product = [Product MR_createInContext:localContext];
                                    }

                                    product.product_app_store_id = obj.productIdentifier;
                                    product.name                 = obj.localizedTitle;
                                    product.desc                 = obj.localizedDescription;

                                    /*City *city;
                                    if ( idx < cities.count ) {
                                        city = cities[ idx ];

                                        id has_product = [City MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"ANY products.product_app_store_id == %@",
                                                                                                                          obj.productIdentifier]
                                                               inContext:localContext];

                                        NSLog(@"");

                                    } else {
                                        city = [City MR_createInContext:localContext];
                                        city.desc   = [NSString stringWithFormat:@"Bu şehir %@ paketi için otomatik yaratıldı",
                                                                                 obj.productIdentifier];
                                        city.title  = [NSString stringWithFormat:@"IAP City %i", idx];
                                        city.visual = @[
                                                @"http://www.bestourism.com/img/items/big/733/Toronto-in-Canada_Toronto-Skyline_2904.jpg",
                                                @"http://i.telegraph.co.uk/multimedia/archive/01552/shard_1552861b.jpg",
                                                @"http://images.businessweek.com/ss/08/06/0625_design_cities/image/intro.jpg"
                                        ][ idx ];

                                        city.products = [NSSet setWithObject:product];

                                        NSLog(@"");
                                    }*/

                                    _apple_products_by_id[ obj.productIdentifier ] = obj;
                                }];

                            }];
                        }

                    });



                    NSLog(@"apple products loaded");

                } failure:^(NSError *error) {
          UIAlertView *alert = [UIAlertView alertViewWithTitle:@"" message:@"Unable to load Apple IAP Products"];
          [alert addButtonWithTitle:@"Ok" handler:^{

          }];
          [alert show];
      }];

  }

  - ( void )buyProduct:( Product * )product {
      SKProduct *apple_product = _apple_products_by_id[ product.product_app_store_id ];

  }
@end
