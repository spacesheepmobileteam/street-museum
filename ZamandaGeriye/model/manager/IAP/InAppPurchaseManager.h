//
// Created by ali kiran on 02/01/14.
// Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RMStore.h"

@interface InAppPurchaseManager : NSObject <NSFetchedResultsControllerDelegate, RMStoreTransactionPersistor>

    + ( InAppPurchaseManager * )instance;

    - ( void )loadProducts;
@end
