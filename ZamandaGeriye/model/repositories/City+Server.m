//
// Created by ali kiran on 29/12/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "City+Server.h"
#import "IAPTransaction.h"
#import "Product.h"

@implementation City(Server)

  + ( NSFetchedResultsController * )citiesWithPredicate:( NSPredicate * )predicate
                                                 target:( id <NSFetchedResultsControllerDelegate> )target
                                            resultBlock:( void (^)(
                                                    NSArray *records, id pageManager, BOOL *requestNextPage) )resultBlock
                                           failureBlock:( void (^)(
                                                   NSError *error) )failureBlock; {
      MMRecordOptions *options = [self defaultOptions];
      options.deleteOrphanedRecordBlock = ^BOOL (MMRecord *orphan, NSArray *populatedRecords, id responseObject, BOOL *stop
      ) {
          NSLog(@"%@", orphan);
          return YES;
      };

      [self startPagedRequestWithURN:@"api/GetCities" data:nil
                                                      context:[NSManagedObjectContext MR_defaultContext] domain:self
                                                      resultBlock:resultBlock failureBlock:failureBlock];

      return [City MR_fetchAllGroupedBy:nil withPredicate:predicate sortedBy:@"title" ascending:NO
                                            delegate:target];
  }

@end
