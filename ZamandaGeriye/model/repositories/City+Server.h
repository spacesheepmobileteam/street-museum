//
// Created by ali kiran on 29/12/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "City.h"

@interface City(Server)

    + ( NSFetchedResultsController * )citiesWithPredicate:( NSPredicate * )predicate
                                                   target:( id <NSFetchedResultsControllerDelegate> )target
                                              resultBlock:( void (^)(
                                                      NSArray *records, id pageManager, BOOL *requestNextPage) )resultBlock failureBlock:( void (^)(
            NSError *error) )failureBlock;
@end
