//
// Created by ali kiran on 29/12/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Language.h"

@interface Language(Server)

    + ( NSFetchedResultsController * )languagesWithPredicate:( NSPredicate * )predicate
                                                          target:( id <NSFetchedResultsControllerDelegate> )target
                                                     resultBlock:( void (^)(NSArray *records, id pageManager,
                                                                            BOOL *requestNextPage) )resultBlock
                                                    failureBlock:( void (^)(NSError *error) )failureBlock;
@end
