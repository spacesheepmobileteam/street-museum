//
// Created by ali kiran on 02/01/14.
// Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import "IAPTransaction+Server.h"
#import "Product.h"

@implementation IAPTransaction(Server)

  + ( NSFetchedResultsController * )transactionsForProduct:( Product * )product {
      return [IAPTransaction MR_fetchAllGroupedBy:nil
                             withPredicate:[NSPredicate predicateWithFormat:@"ANY product.identifier ==%@",
                                                                            product.identifier]
                             sortedBy:@"transaction_date" ascending:YES];
  }

  + ( NSFetchedResultsController * )allTransactions {
      return [IAPTransaction MR_fetchAllGroupedBy:@"product.product_app_store_id" withPredicate:nil
                                                                                  sortedBy:@"transaction_date"
                                                                                  ascending:YES];

  }
@end
