//
// Created by ali kiran on 29/12/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "Location+Server.h"
#import <CoreLocation/CoreLocation.h>
#import <RCLocationManager/RCLocationManager.h>

@implementation Location(Server)

  + ( NSFetchedResultsController * )locationsWithPredicate:( NSPredicate * )predicate target:( id <NSFetchedResultsControllerDelegate> )target resultBlock:( void (^)(NSArray *records, id pageManager, BOOL *requestNextPage) )resultBlock failureBlock:( void (^)(NSError *error) )failureBlock; {
      MMRecordOptions *options = [self defaultOptions];
      options.deleteOrphanedRecordBlock = ^BOOL (MMRecord *orphan, NSArray *populatedRecords, id responseObject, BOOL *stop) {
          NSLog(@"%@", orphan);
          return YES;
      };

      [self startPagedRequestWithURN:@"api/GetLocations" data:nil
                                                         context:[NSManagedObjectContext MR_defaultContext] domain:self
                                                         resultBlock:resultBlock failureBlock:failureBlock];

      return [Location MR_fetchAllGroupedBy:@"city_name" withPredicate:predicate sortedBy:@"title" ascending:NO
                                                         delegate:target];
  }

  + ( void )nearbyLocationsWithCLLocationPredicate:( NSPredicate * )predicate target:( id <NSFetchedResultsControllerDelegate> )target resultBlock:( void (^)(NSFetchedResultsController *resultsController) )resultBlock failureBlock:( void (^)(NSError *error) )failureBlock; {

      RCLocationManager *locationManager = [RCLocationManager sharedManager];
      [locationManager retriveUserLocationWithBlock:^(CLLocationManager *manager, CLLocation *newLocation, CLLocation *oldLocation) {
          MMRecordOptions *options = [self defaultOptions];
          options.deleteOrphanedRecordBlock = ^BOOL (MMRecord *orphan, NSArray *populatedRecords, id responseObject, BOOL *stop) {
              NSLog(@"%@", orphan);
              return YES;
          };

          NSFetchedResultsController *cached_locations = [Location MR_fetchAllGroupedBy:nil withPredicate:predicate
                                                                                            sortedBy:@"title"
                                                                                            ascending:NO
                                                                                            delegate:target];
          resultBlock(cached_locations);

          [self startPagedRequestWithURN:[NSString stringWithFormat:@"api/GetLocations?Latitude=%f&Longitude=%f",
                                                                    newLocation.coordinate.latitude,
                                                                    newLocation.coordinate.longitude] data:nil
                                                                                                      context:[NSManagedObjectContext MR_defaultContext]
                                                                                                      domain:self
                                                                                                      resultBlock:^(NSArray *records, id pageManager, BOOL *requestNextPage) {
                                                                                                          NSLog(@"");
                                                                                                      }
                                                                                                      failureBlock:failureBlock];

      } errorBlock:^(CLLocationManager *manager, NSError *error) {

      }];

  }

  - ( NSString * )recordDetailURN; {
      return [NSString stringWithFormat:@"api/GetDetail?%@Id", self.identifier];
  }

@end
