//
//  City.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 03/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"
@class Location, Product, User;

@interface City : BaseEntity

@property (nonatomic, retain) NSNumber * artist_count;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * legend_count;
@property (nonatomic, retain) NSNumber * like_count;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSNumber * news_count;
@property (nonatomic, retain) NSNumber * photo_count;
@property (nonatomic, retain) NSNumber * place_count;
@property (nonatomic, retain) NSString * share_text;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * visual;
@property (nonatomic, retain) NSSet *locations;
@property (nonatomic, retain) NSSet *products;
@property (nonatomic, retain) NSSet *users;
@end

@interface City (CoreDataGeneratedAccessors)

- (void)addLocationsObject:(Location *)value;
- (void)removeLocationsObject:(Location *)value;
- (void)addLocations:(NSSet *)values;
- (void)removeLocations:(NSSet *)values;

- (void)addProductsObject:(Product *)value;
- (void)removeProductsObject:(Product *)value;
- (void)addProducts:(NSSet *)values;
- (void)removeProducts:(NSSet *)values;

- (void)addUsersObject:(User *)value;
- (void)removeUsersObject:(User *)value;
- (void)addUsers:(NSSet *)values;
- (void)removeUsers:(NSSet *)values;

@end
