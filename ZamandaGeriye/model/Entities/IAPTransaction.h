//
//  IAPTransaction.h
//  ZamandaGeriye
//
//  Created by ali kiran on 02/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"

@class Product, User;

@interface IAPTransaction : BaseEntity

    @property(nonatomic, retain) NSNumber *quantity;
    @property(nonatomic, retain) NSData   *transaction_receipt;
    @property(nonatomic, retain) NSDate   *transaction_date;
    @property(nonatomic, retain) NSString *transaction_identifier;
    @property(nonatomic, retain) Product *product;
    @property(nonatomic, retain) User    *user;

@end
