//
//  Tag.m
//  ZamandaGeriye
//
//  Created by ali kiran on 30/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "Tag.h"
#import "Location.h"
#import "News.h"


@implementation Tag

@dynamic identifier;
@dynamic title;
@dynamic locations;
@dynamic news;

@end
