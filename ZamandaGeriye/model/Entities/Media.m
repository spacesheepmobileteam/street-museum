//
//  Media.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 03/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import "Media.h"
#import "Location.h"
#import "News.h"


@implementation Media

@dynamic desc;
@dynamic identifier;
@dynamic source;
@dynamic title;
@dynamic location;
@dynamic news;

@end
