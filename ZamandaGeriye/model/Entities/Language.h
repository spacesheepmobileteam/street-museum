//
//  Language.h
//  ZamandaGeriye
//
//  Created by ali kiran on 30/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"

@class User;

@interface Language : BaseEntity

@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) User *user;

@end
