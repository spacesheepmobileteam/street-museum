//
//  Tag.h
//  ZamandaGeriye
//
//  Created by ali kiran on 30/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"

@class Location, News;

@interface Tag : BaseEntity

@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Location *locations;
@property (nonatomic, retain) NSSet *news;
@end

@interface Tag (CoreDataGeneratedAccessors)

- (void)addNewsObject:(News *)value;
- (void)removeNewsObject:(News *)value;
- (void)addNews:(NSSet *)values;
- (void)removeNews:(NSSet *)values;

@end
