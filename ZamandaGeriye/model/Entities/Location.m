//
//  Location.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 03/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import "Location.h"
#import "Area.h"
#import "City.h"
#import "District.h"
#import "LocationType.h"
#import "Media.h"
#import "Neighborhood.h"
#import "Tag.h"
#import "User.h"


@implementation Location

@dynamic address;
@dynamic architect_name;
@dynamic build_year;
@dynamic city_name;
@dynamic create_date;
@dynamic desc;
@dynamic detail_id;
@dynamic entrance_fee;
@dynamic founder;
@dynamic identifier;
@dynamic latitude;
@dynamic like_count;
@dynamic longitude;
@dynamic marker_image;
@dynamic open_days;
@dynamic open_hours;
@dynamic postcode;
@dynamic spot_text;
@dynamic sultan_name;
@dynamic text;
@dynamic title;
@dynamic update_date;
@dynamic area;
@dynamic city;
@dynamic district;
@dynamic media;
@dynamic neighborhood;
@dynamic tags;
@dynamic types;
@dynamic users;

@end
