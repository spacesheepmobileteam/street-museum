//
//  User.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 03/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import "User.h"
#import "City.h"
#import "IAPTransaction.h"
#import "Language.h"
#import "Location.h"


@implementation User

@dynamic identifier;
@dynamic language;
@dynamic transactions;
@dynamic favorite_locations;
@dynamic favorite_cities;

@end
