//
//  LocationType.m
//  ZamandaGeriye
//
//  Created by ali kiran on 01/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import "LocationType.h"
#import "Location.h"


@implementation LocationType

@dynamic identifier;
@dynamic language;
@dynamic title;
@dynamic selected;
@dynamic locations;

@end
