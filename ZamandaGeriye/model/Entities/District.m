//
//  District.m
//  ZamandaGeriye
//
//  Created by ali kiran on 30/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "District.h"
#import "Location.h"


@implementation District

@dynamic identifier;
@dynamic title;
@dynamic location;

@end
