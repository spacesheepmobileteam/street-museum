//
//  Media.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 03/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"

@class Location, News;

@interface Media : BaseEntity

    @property(nonatomic, retain) NSString *desc;
    @property(nonatomic, retain) NSNumber *identifier;
    @property(nonatomic, retain) NSString *source;
    @property(nonatomic, retain) NSString *title;
    @property(nonatomic, retain) NSSet *location;
    @property(nonatomic, retain) NSSet *news;
@end

@interface Media(CoreDataGeneratedAccessors)

    - ( void )addLocationObject:( Location * )value;
    - ( void )removeLocationObject:( Location * )value;
    - ( void )addLocation:( NSSet * )values;
    - ( void )removeLocation:( NSSet * )values;

    - ( void )addNewsObject:( News * )value;
    - ( void )removeNewsObject:( News * )value;
    - ( void )addNews:( NSSet * )values;
    - ( void )removeNews:( NSSet * )values;

@end
