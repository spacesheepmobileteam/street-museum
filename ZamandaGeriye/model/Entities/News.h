//
//  News.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 03/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"
@class Media, Tag;

@interface News : BaseEntity

@property (nonatomic, retain) NSDate * create_date;
@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSString * spot_text;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * thumb;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *media;
@property (nonatomic, retain) NSSet *tags;
@end

@interface News (CoreDataGeneratedAccessors)

- (void)addMediaObject:(Media *)value;
- (void)removeMediaObject:(Media *)value;
- (void)addMedia:(NSSet *)values;
- (void)removeMedia:(NSSet *)values;

- (void)addTagsObject:(Tag *)value;
- (void)removeTagsObject:(Tag *)value;
- (void)addTags:(NSSet *)values;
- (void)removeTags:(NSSet *)values;

@end
