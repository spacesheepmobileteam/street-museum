//
//  IAPTransaction.m
//  ZamandaGeriye
//
//  Created by ali kiran on 02/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import "IAPTransaction.h"
#import "Product.h"
#import "User.h"

@implementation IAPTransaction

  @dynamic quantity;
  @dynamic transaction_receipt;
  @dynamic transaction_date;
  @dynamic transaction_identifier;
  @dynamic product;
  @dynamic user;

@end
