//
//  LocationType.h
//  ZamandaGeriye
//
//  Created by ali kiran on 01/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"

@class Location;

@interface LocationType : BaseEntity

    @property(nonatomic, retain) NSNumber *identifier;
    @property(nonatomic, retain) NSString *language;
    @property(nonatomic, retain) NSString *title;
    @property(nonatomic, retain) NSNumber *selected;
    @property(nonatomic, retain) NSSet    *locations;
@end

@interface LocationType(CoreDataGeneratedAccessors)

    - ( void )addLocationsObject:( Location * )value;
    - ( void )removeLocationsObject:( Location * )value;
    - ( void )addLocations:( NSSet * )values;
    - ( void )removeLocations:( NSSet * )values;

@end
