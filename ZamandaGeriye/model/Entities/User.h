//
//  User.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 03/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"


@class City, IAPTransaction, Language, Location;

@interface User : BaseEntity

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) Language *language;
@property (nonatomic, retain) NSSet *transactions;
@property (nonatomic, retain) NSSet *favorite_locations;
@property (nonatomic, retain) NSSet *favorite_cities;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addTransactionsObject:(IAPTransaction *)value;
- (void)removeTransactionsObject:(IAPTransaction *)value;
- (void)addTransactions:(NSSet *)values;
- (void)removeTransactions:(NSSet *)values;

- (void)addFavorite_locationsObject:(Location *)value;
- (void)removeFavorite_locationsObject:(Location *)value;
- (void)addFavorite_locations:(NSSet *)values;
- (void)removeFavorite_locations:(NSSet *)values;

- (void)addFavorite_citiesObject:(City *)value;
- (void)removeFavorite_citiesObject:(City *)value;
- (void)addFavorite_cities:(NSSet *)values;
- (void)removeFavorite_cities:(NSSet *)values;

@end
