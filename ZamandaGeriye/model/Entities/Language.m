//
//  Language.m
//  ZamandaGeriye
//
//  Created by ali kiran on 30/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "Language.h"
#import "User.h"


@implementation Language

@dynamic identifier;
@dynamic title;
@dynamic user;

@end
