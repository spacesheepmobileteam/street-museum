//
//  City.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 03/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import "City.h"
#import "Location.h"
#import "Product.h"
#import "User.h"


@implementation City

@dynamic artist_count;
@dynamic desc;
@dynamic identifier;
@dynamic latitude;
@dynamic legend_count;
@dynamic like_count;
@dynamic longitude;
@dynamic news_count;
@dynamic photo_count;
@dynamic place_count;
@dynamic share_text;
@dynamic title;
@dynamic visual;
@dynamic locations;
@dynamic products;
@dynamic users;

@end
