//
//  Product.h
//  ZamandaGeriye
//
//  Created by ali kiran on 02/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"

@class City, IAPTransaction;

@interface Product : BaseEntity

    @property(nonatomic, retain) NSString *apple_id;
    @property(nonatomic, retain) NSNumber *identifier;
    @property(nonatomic, retain) NSString *name;
    @property(nonatomic, retain) NSString *product_app_store_id;
    @property(nonatomic, retain) NSString *desc;
    @property(nonatomic, retain) NSSet *cities;
    @property(nonatomic, retain) NSSet *transactions;
@end

@interface Product(CoreDataGeneratedAccessors)

    - ( void )addCitiesObject:( City * )value;
    - ( void )removeCitiesObject:( City * )value;
    - ( void )addCities:( NSSet * )values;
    - ( void )removeCities:( NSSet * )values;

    - ( void )addTransactionsObject:( IAPTransaction * )value;
    - ( void )removeTransactionsObject:( IAPTransaction * )value;
    - ( void )addTransactions:( NSSet * )values;
    - ( void )removeTransactions:( NSSet * )values;

@end
