//
//  Area.m
//  ZamandaGeriye
//
//  Created by ali kiran on 30/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "Area.h"
#import "Location.h"


@implementation Area

@dynamic identifier;
@dynamic postcode;
@dynamic title;
@dynamic location;

@end
