//
//  News.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 03/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import "News.h"
#import "Media.h"
#import "Tag.h"


@implementation News

@dynamic create_date;
@dynamic identifier;
@dynamic spot_text;
@dynamic text;
@dynamic thumb;
@dynamic title;
@dynamic media;
@dynamic tags;

@end
