//
//  Location.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 03/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"

@class Area, City, District, LocationType, Media, Neighborhood, Tag, User;

@interface Location : BaseEntity

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * architect_name;
@property (nonatomic, retain) NSNumber * build_year;
@property (nonatomic, retain) NSString * city_name;
@property (nonatomic, retain) NSDate * create_date;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * detail_id;
@property (nonatomic, retain) NSString * entrance_fee;
@property (nonatomic, retain) NSString * founder;
@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * like_count;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * marker_image;
@property (nonatomic, retain) NSString * open_days;
@property (nonatomic, retain) NSString * open_hours;
@property (nonatomic, retain) NSString * postcode;
@property (nonatomic, retain) NSString * spot_text;
@property (nonatomic, retain) NSString * sultan_name;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSDate * update_date;
@property (nonatomic, retain) Area *area;
@property (nonatomic, retain) City *city;
@property (nonatomic, retain) District *district;
@property (nonatomic, retain) NSSet *media;
@property (nonatomic, retain) Neighborhood *neighborhood;
@property (nonatomic, retain) Tag *tags;
@property (nonatomic, retain) NSSet *types;
@property (nonatomic, retain) NSSet *users;
@end

@interface Location (CoreDataGeneratedAccessors)

- (void)addMediaObject:(Media *)value;
- (void)removeMediaObject:(Media *)value;
- (void)addMedia:(NSSet *)values;
- (void)removeMedia:(NSSet *)values;

- (void)addTypesObject:(LocationType *)value;
- (void)removeTypesObject:(LocationType *)value;
- (void)addTypes:(NSSet *)values;
- (void)removeTypes:(NSSet *)values;

- (void)addUsersObject:(User *)value;
- (void)removeUsersObject:(User *)value;
- (void)addUsers:(NSSet *)values;
- (void)removeUsers:(NSSet *)values;

@end
