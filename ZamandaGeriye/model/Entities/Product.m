//
//  Product.m
//  ZamandaGeriye
//
//  Created by ali kiran on 02/01/14.
//  Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import "Product.h"
#import "City.h"
#import "IAPTransaction.h"


@implementation Product

@dynamic apple_id;
@dynamic identifier;
@dynamic name;
@dynamic product_app_store_id;
@dynamic desc;
@dynamic cities;
@dynamic transactions;

@end
