//
// Created by ali kiran on 29/12/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "BaseEntity.h"

NSDateFormatter *ADNRecordDateFormatter;

@implementation BaseEntity {

  }

  + ( NSString * )keyPathForResponseObject {
      return @"Data";
  }

  + ( NSDateFormatter * )dateFormatter; {
      if ( !ADNRecordDateFormatter ) {
          NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
          [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"]; // "2012-11-21T03:57:39Z"
          ADNRecordDateFormatter = dateFormatter;
      }

      return ADNRecordDateFormatter;
  }

@end
