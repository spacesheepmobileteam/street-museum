//
//  Neighborhood.m
//  ZamandaGeriye
//
//  Created by ali kiran on 30/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "Neighborhood.h"
#import "Location.h"


@implementation Neighborhood

@dynamic identifier;
@dynamic title;
@dynamic location;

@end
