//
// Created by Serdar Yıllar on 27/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPClient.h>
#import <AFNetworking/AFJSONRequestOperation.h>
#import <StoreKit/StoreKit.h>

@class Language;
@class User;

@interface MainModel : NSObject

    + ( MainModel * )instance;
    @property(nonatomic, strong) Language *language;
    @property(nonatomic, strong) User *user;
@end
