// MMAFJSONServer.m
//
// Copyright (c) 2013 Mutual Mobile (http://www.mutualmobile.com/)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "MainJSONServer.h"

#import <objc/runtime.h>
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "MainHTTPClient.h"

@implementation MainJSONServer

  + ( void )cancelRequestsWithDomain:( id )domain {
      MainHTTPClient *client = [MainHTTPClient instance];

      if ( domain ) {
          for ( NSOperation *operation in [client.operationQueue operations] ) {
              if ( ![operation isKindOfClass:[AFJSONRequestOperation class]] ) {
                  continue;
              }

              Class domainClass = [domain class];

              if ( [[(AFJSONRequestOperation *) operation requestOperationDomain]
                                                          isEqualToString:NSStringFromClass(domainClass)] ) {
                  [operation cancel];
              }
          }
      }
  }

  + ( void )startRequestWithURN:( NSString * )URN data:( NSDictionary * )data paged:( BOOL )paged domain:( id )domain batched:( BOOL )batched dispatchGroup:( dispatch_group_t )dispatchGroup responseBlock:( void (^)(id responseObject) )responseBlock failureBlock:( void (^)(NSError *error) )failureBlock {

      if ( paged ) {

      }

      NSMutableURLRequest *baseRequest = [[MainHTTPClient instance] requestWithURN:URN data:data domain:domain];

      AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:baseRequest
                                                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                                      responseBlock(JSON);
                                                                  }
                                                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                      failureBlock(error);
                                                                  }];

      if ( domain ) {
          Class domainClass = [domain class];

          [operation setRequestOperationDomain:NSStringFromClass(domainClass)];
      }

      [[MainHTTPClient instance] enqueueHTTPRequestOperation:operation];
  }

  + ( Class )pageManagerClass {
      return nil;
  }

@end

@implementation AFJSONRequestOperation(MainJSONServer)

  - ( id )requestOperationDomain {
      return objc_getAssociatedObject(self, @"MMRecord_recordClassName");
  }

  - ( void )setRequestOperationDomain:( id )domain {
      objc_setAssociatedObject(self, @"MMRecord_recordClassName", domain, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  }

@end
