//
// Created by ali kiran on 29/12/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPClient.h"

@interface MainHTTPClient : AFHTTPClient

	+ ( MainHTTPClient * )instance;

	- ( NSMutableURLRequest * )requestWithURN:( NSString * )URN data:( NSDictionary * )data domain:( id )domain;
@end
