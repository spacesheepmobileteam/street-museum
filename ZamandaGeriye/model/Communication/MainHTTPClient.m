//
// Created by ali kiran on 29/12/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <AFNetworking/AFJSONRequestOperation.h>
#import "MainHTTPClient.h"

@implementation MainHTTPClient {

		NSInteger _count;
	}

	+ ( MainHTTPClient * )instance {
		static MainHTTPClient *_instance = nil;

		@synchronized ( self ) {
			if ( _instance == nil) {

				_instance = [[self alloc] initWithBaseURL:[NSURL URLWithString:DOMAIN_URL]];
			}
		}

		return _instance;
	}

	- ( id )initWithBaseURL:( NSURL * )url; {
		self = [super initWithBaseURL:url];
		if ( self ) {
			_count = 0;
			[self registerHTTPOperationClass:[AFJSONRequestOperation class]];
//			[self setDefaultHeader:@"Accept" value:@"application/json"];
		}

		return self;
	}

	- ( NSString * )token {
		return @"";
	}

	- ( NSMutableURLRequest * )requestWithURN:( NSString * )URN data:( NSDictionary * )data domain:( id )domain; {
		NSLog(@"%@", domain);
		_count++;

/*		NSMutableURLRequest *request;
		if ( [URN.entity.name.lowercaseString isEqualToString:@"feed"] ) {
			NSMutableDictionary *mutableParameters = [NSMutableDictionary dictionary];
			NSString            *request_path;

			request_path = [self getRequestIDWithEntityName:URN.entity.name fetchRequest:URN];

			request = [self requestWithMethod:@"GET" path:request_path
								   parameters:[mutableParameters count] == 0 ? nil : mutableParameters];

		} else if ( [URN.entity.name.lowercaseString isEqualToString:@"city"] ) {
			request = [self requestWithMethod:@"GET" path:@"api/GetCities" parameters:nil];
		} else if ( [URN.entity.name.lowercaseString isEqualToString:@"language"] ) {
			request = [self requestWithMethod:@"GET" path:@"api/GetLanguages" parameters:nil];
		} else if ( [URN.entity.name.lowercaseString isEqualToString:@"product"] ) {
			request = [self requestWithMethod:@"GET" path:@"api/GetProducts" parameters:nil];
		} else {
			request = [super requestWithURN:URN data:data domain:domain];
		}*/

		//
		NSString *urn_escaped = [URN stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

		NSMutableURLRequest *request = [super requestWithMethod:@"GET" path:urn_escaped parameters:nil];
		[request addValue:self.token forHTTPHeaderField:@"token"];

		return request;
	}
@end
