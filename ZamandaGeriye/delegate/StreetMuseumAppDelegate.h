//
//  StreetMuseumAppDelegate.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 11/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainModel.h"

@interface StreetMuseumAppDelegate : UIResponder <UIApplicationDelegate>

    @property(strong, nonatomic) UIWindow *window;

@end
