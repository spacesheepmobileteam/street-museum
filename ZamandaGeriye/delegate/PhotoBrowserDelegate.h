//
// Created by Serdar Yıllar on 15/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>

@protocol PhotoBrowserDelegate <NSObject>

-(void) openPhotoBrowserWithArray:(NSArray *)_array;

@end
