//
// Created by Serdar Yıllar on 12/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>

@class City;

@protocol CityCarouselViewDelegate <NSObject>

    - ( NSInteger )getCityCount:( UIView * )view;
    - ( City * )getCityItem:( int )path at:( UIView * )at;
    - ( void )action_view_item_at:( UIView * )view;
    - ( void )action_buy_item_at:( UIView * )view;
    - ( BOOL )hasPurchased:( City * )city;
@end
