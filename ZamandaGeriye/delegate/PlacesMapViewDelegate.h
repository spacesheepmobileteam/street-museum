//
// Created by Serdar Yıllar on 03/12/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>

@class PlacesMapView;

@protocol PlacesMapViewDelegate <NSObject>

	- ( NSInteger )getAnnotationCount:( PlacesMapView * )view;

	- ( NSArray * )getAnnotationArray;
@end
