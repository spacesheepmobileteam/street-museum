//
// Created by Serdar Yıllar on 29/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>

@class NewsView;
@class News;

@protocol NewsViewDelegate <NSObject>

	- ( News * )getNewsItem:( int )path at:( NewsView * )at;

	- ( NSInteger )getNewsCount:( NewsView * )view;

@end
