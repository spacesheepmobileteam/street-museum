//
// Created by Serdar Yıllar on 09/12/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>

@protocol PlacesListCellViewDelegate <NSObject>

	- ( void )callLocateAtMapWithId:( int )cellIndex;
@end
