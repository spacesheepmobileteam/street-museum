//
//  StreetMuseumAppDelegate.m
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 11/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "MainModel.h"
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>
#import "StreetMuseumAppDelegate.h"
#import "Harpy.h"
#import "GAILogger.h"
#import "GAI.h"
#import "ARAnalytics.h"
#import "GTSocialManager.h"
#import "FBSession+Protected.h"
#import "Foursquare2.h"
#import "FBAppEvents.h"
#import "TMCache.h"
#import "Reachability.h"
#import "UIColor+Hex.h"
#import "AFHTTPRequestOperationLogger.h"
#import "MMRecord.h"
#import "MainJSONServer.h"
#import "StubManager.h"
#import "Media.h"
#import "News.h"
#import <GoogleOpenSource/GoogleOpenSource.h>

MainModel *main_model;

@implementation StreetMuseumAppDelegate {
      NetworkStatus _network_status;
      id            _receiptVerificator;
      BOOL          _productsRequestFinished;
      id            _main_persistance;
  }

  - ( BOOL )application:( UIApplication * )application didFinishLaunchingWithOptions:( NSDictionary * )launchOptions {

      self.window.backgroundColor = [UIColor colorWithHex:0x494a4a];
      [[UINavigationBar appearance] setTitleTextAttributes:@{
              NSForegroundColorAttributeName : [UIColor whiteColor],
              NSFontAttributeName            : [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0]
      }];

      NSLog(@"AppName %@", AppName);
      NSLog(@"IOSVersion %@", IOSVersion);
      NSLog(@"AppVersion %@", AppVersion);

      [self configureTracking];
      [self configureNetwork];
      [self configureSocialServices];
      [self configureVersioning];
      [self configurePersistance];
      [self configureAppStore];
      [self configureNotifications];

      NSArray *all_media = [Media MR_findAll];
      NSArray *all_news = [News MR_findAll];

      self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
      [self.window makeKeyAndVisible];

//      [[InAppPurchaseManager instance] loadProducts];

      /* [MagicalRecord saveUsingCurrentThreadContextWithBlockAndWait:^(NSManagedObjectContext *localContext) {
           NSArray *all_server_products = [Product MR_findAllInContext:localContext];
           NSLog(@"");

           [all_server_products enumerateObjectsUsingBlock:^(Product *obj, NSUInteger idx, BOOL *stop) {
               obj.cities = nil;

           }];

       }];*/


      UIStoryboard     *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
      UIViewController *vc;

      if ( main_model.language ) {
          vc = [storyboard instantiateViewControllerWithIdentifier:@"story_root"];

      } else {
          vc = [storyboard instantiateViewControllerWithIdentifier:@"story_landscape"];
      }

      [self.window setRootViewController:vc];

      return YES;

  }

  - ( void )configureNotifications; {
      [[UIApplication sharedApplication] cancelAllLocalNotifications];
      [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
  }

  - ( void )configureSocialServices; {
      [[GTSocialManager instance] configure];
  }

  - ( void )configureVersioning; {
      NSString *versionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *) kCFBundleVersionKey];

      [[Harpy sharedInstance] setAppID:versionString];
      [[Harpy sharedInstance] setAlertType:HarpyAlertTypeOption];
      [[Harpy sharedInstance] checkVersion];
  }

  - ( void )configureNetwork; {
      Reachability *reachability = [Reachability reachabilityForInternetConnection];
      [reachability startNotifier];

      _network_status = reachability.currentReachabilityStatus;

      NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:8 * 1024 * 1024 diskCapacity:20 * 1024 * 1024
                                                                                        diskPath:nil];
      [NSURLCache setSharedURLCache:URLCache];

      [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];

      [AFHTTPRequestOperationLogger sharedLogger].level = AFLoggerLevelDebug;
      [[AFHTTPRequestOperationLogger sharedLogger] startLogging];

      [StubManager start];

  }

  - ( void )configureTracking; {
      [ARAnalytics setupWithAnalytics:@{
              ARGoogleAnalyticsID : @"UA-303852-23", ARTestFlightAppToken : @"2e5ae5f8-e1c2-428f-86cc-d252e37100e0"
      }];

      [GAI sharedInstance].trackUncaughtExceptions = NO;
      [[GAI sharedInstance] setDryRun:NO];
      [GAI sharedInstance].dispatchInterval = 20;

      // Optional: set Logger to VERBOSE for debug information.
      [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelError];

      [TestFlight setOptions:@{ TFOptionLogToConsole : @NO }];
      [TestFlight setOptions:@{ TFOptionLogToSTDERR : @NO }];
  }

  - ( void )configurePersistance {
      main_model = [MainModel new];
      [MMRecord setLoggingLevel:MMRecordLoggingLevelAll];

      [MMRecord registerServerClass:[MainJSONServer class]];

  }

  - ( void )application:( UIApplication * )application didReceiveRemoteNotification:( NSDictionary * )userInfo {
      NSLog(@"");
  }

  - ( void )                         application:( UIApplication * )application
didFailToRegisterForRemoteNotificationsWithError:( NSError * )error {
      NSLog(@"");

      UIAlertView *alertView = [[UIAlertView alloc]
                                             initWithTitle:NSLocalizedString(@"Remote Notification Fail", @"Remote Notification Fail")
                                             message:error.userInfo.debugDescription delegate:nil
                                             cancelButtonTitle:@"Ok" otherButtonTitles:nil];
      [alertView show];
  }

  - ( void )                         application:( UIApplication * )application
didRegisterForRemoteNotificationsWithDeviceToken:( NSData * )deviceToken {
      NSString *dToken = [[deviceToken description]
                                       stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
      dToken = [dToken stringByReplacingOccurrencesOfString:@" " withString:@""];



      NSLog(@"remote device token %@", dToken);
  }

  - ( void )    application:( UIApplication * )application
didReceiveLocalNotification:( UILocalNotification * )notification {
      NSLog(@"");
  }

  - ( void )renderCityInterface; {

      UIStoryboard           *authStoryboard       = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
      UINavigationController *navigationController = [authStoryboard instantiateInitialViewController];

      self.window.rootViewController = navigationController;
  }

  - ( void )configureAppStore; {

  }

  - ( void )controllerDidChangeContent:( NSFetchedResultsController * )controller; {
      NSLog(@"");
  }

  - ( void )applicationWillTerminate:( UIApplication * )application {

      [FBSession.activeSession close];
  }

  - ( BOOL )application:( UIApplication * )application
                openURL:( NSURL * )url
      sourceApplication:( NSString * )sourceApplication
             annotation:( id )annotation {
      NSLog(@"");
      if ( [FBSession.activeSession handleOpenURL:url] ) {
          return YES;
      } else if ( [Foursquare2 handleURL:url] ) {
          return YES;
      } else if ( [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation] ) {
          return YES;
      }

      return NO;
  }

  - ( void )applicationDidBecomeActive:( UIApplication * )application {

      [[Harpy sharedInstance] checkVersionWeekly];

      [FBAppEvents activateApp];
      [FBSession.activeSession handleDidBecomeActive];

  }

// User presented with update dialog
  - ( void )harpyDidShowUpdateDialog {

  }

// User did click on button that launched App Store.app
  - ( void )harpyUserDidLaunchAppStore {

  }

// User did click on button that skips version update
  - ( void )harpyUserDidSkipVersion {

  }

// User did click on button that cancels update dialog
  - ( void )harpyUserDidCancel {

  }

  - ( void )applicationDidReceiveMemoryWarning:( UIApplication * )application; {
      [[NSURLCache sharedURLCache] removeAllCachedResponses];
      [[TMCache sharedCache] removeAllObjects];
  }

@end
