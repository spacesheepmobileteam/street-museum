//
// Created by Serdar Yıllar on 15/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>

@class PlacesListView;
@class Location;

@protocol PlacesListViewDelegate <NSObject>

	- ( NSMutableArray * )getGroupedLocationsArray:( int )path at:( PlacesListView * )at;

	- ( NSInteger )getLocationsCount:( PlacesListView * )view;

	- ( NSInteger )getGroupedLocationsCount:( PlacesListView * )view;

	- ( Location * )getLocationsItem:( int )path at:( PlacesListView * )at;

	- ( void )openLocationOnMapWithCellIndex:( int )index;
@end
