//
//  SelectedPackage.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 13/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectedPackage : NSObject

	@property(nonatomic, strong) NSMutableDictionary *selectedPackages;

	+ ( SelectedPackage * )instance;

	- ( NSMutableDictionary * )getSelectedPackage;
@end
