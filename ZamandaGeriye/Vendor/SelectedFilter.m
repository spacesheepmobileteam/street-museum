//
//  SelectedFilter.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 13/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "SelectedFilter.h"

@implementation SelectedFilter

	+ ( SelectedFilter * )instance {
		static SelectedFilter *_instance = nil;

		@synchronized ( self ) {
			if ( _instance == nil) {
				_instance = [[self alloc] init];

			}
		}
		return _instance;

	}

	- ( void )clearAllItems {
		if(_activeFilterDict){
			[_activeFilterDict removeAllObjects];
		}
	}

	- ( void )setFilterWithName:( NSString * )filterName FilterID:( int )filter_id Language:( int )language_id {

		if ( !_activeFilterDict ) {
			_activeFilterDict = [NSMutableDictionary new];
		}

		if ( _activeFilterDict[ filterName ] ) {
			[_activeFilterDict removeObjectForKey:filterName];

		} else {
			NSArray *array = [[NSArray alloc]
									   initWithObjects:filterName, [NSNumber numberWithInt:filter_id], [NSNumber numberWithInt:language_id], nil];
			[_activeFilterDict setObject:array forKey:filterName];
		}

	}

	- ( NSMutableDictionary * )getFilterList {
		return _activeFilterDict;
	}

@end
