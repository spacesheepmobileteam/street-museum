//
//  SelectedFilter.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 13/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectedFilter : NSObject

	+ ( SelectedFilter * )instance;

	- ( void )clearAllItems;

	- ( void )setFilterWithName:( NSString * )filterName FilterID:( int )filter_id Language:( int )language_id;

	- ( NSMutableDictionary * )getFilterList;

	@property(nonatomic, strong) NSMutableDictionary *activeFilterDict;

	@property(nonatomic) BOOL isNearby;
	@property(nonatomic) BOOL isSelectedAll;
	@property(nonatomic) BOOL isSearchParameter;
	@property(nonatomic, copy) NSString *searchParameter;
@end
