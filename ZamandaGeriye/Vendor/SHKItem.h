//
// Created by Serdar Yıllar on 22/11/13.
// Copyright (c) 2013 Ali Kıran. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <Foursquare-API-v2/Foursquare2.h>

@class CLLocation;

@interface SHKItem : NSObject

	@property(nonatomic, strong) id action_name;
	@property(nonatomic, strong) id action_link;
	@property(nonatomic, strong) id title;
	@property(nonatomic, strong) id facebookURLSharePictureURI;
	@property(nonatomic, strong) CLLocation *location;
	@property(nonatomic, copy) NSString *venue_id;
	@property(nonatomic, copy) NSString *event_id;
	@property(nonatomic, copy) NSString *shout;
	@property(nonatomic) enum FoursquareBroadcastType broadcast;
	@property(nonatomic, strong) NSURL *URL;
	@property(nonatomic, copy) NSString *text;
@end
