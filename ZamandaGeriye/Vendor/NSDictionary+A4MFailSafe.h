//
// Created by ali kiran on 02/12/13.
// Copyright (c) 2013 ali kiran. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSDictionary(A4MFailSafe)

	- ( NSString * )fail_safe_string:( NSString * )string;

	- ( NSDictionary * )fail_safe_dictionary:( NSString * )string;

	- ( NSArray * )fail_safe_array:( NSString * )string;
@end
