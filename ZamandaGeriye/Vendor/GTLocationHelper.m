//
// Created by ali kiran on 7/6/13.
// Copyright (c) 2013 Spacesheep. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "GTLocationHelper.h"
#import "RCLocationManager.h"

@implementation GTLocationHelper {

	}

	+ ( void )openMapsWithDirectionsTo:( CLLocationCoordinate2D )to {
		RCLocationManager *locationManager = [RCLocationManager sharedManager];
		[locationManager retriveUserLocationWithBlock:^(CLLocationManager *manager, CLLocation *newLocation, CLLocation *oldLocation) {
			NSString *url = [self getNativeMapDirection:newLocation.coordinate newLocation:to];

			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

		}                                  errorBlock:^(CLLocationManager *manager, NSError *error) {

		}];
	}

	+ ( void )getTwoLocationDistance:( CLLocationCoordinate2D )to callback:( void (^)(CLLocationDistance) )callback; {

		RCLocationManager *locationManager = [RCLocationManager sharedManager];

		[locationManager retriveUserLocationWithBlock:^(CLLocationManager *manager, CLLocation *newLocation, CLLocation *oldLocation) {

			CLLocation *locA = [[CLLocation alloc]
											initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
			CLLocation *locB = [[CLLocation alloc] initWithLatitude:to.latitude longitude:to.longitude];

			callback([locA distanceFromLocation:locB]);

		}                                  errorBlock:^(CLLocationManager *manager, NSError *error) {
			callback(0);

		}];

	}

	+ ( NSString * )getNativeMapDirection:( CLLocationCoordinate2D )from newLocation:( CLLocationCoordinate2D )to; {
		NSString *versionNum      = [[UIDevice currentDevice] systemVersion];
		NSString *nativeMapScheme = @"maps.apple.com";

		if ( [versionNum compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending ) {
			nativeMapScheme = @"maps.google.com";
		}

		NSString *url = [NSString stringWithFormat:@"http://%@/maps?saddr=%f,%f&daddr=%f,%f", nativeMapScheme, from.latitude, from.longitude, to.latitude, to.longitude];
		return url;
	}

	+ ( NSString * )getNativeMapQuery:( NSString * )where {
		NSString *versionNum      = [[UIDevice currentDevice] systemVersion];
		NSString *nativeMapScheme = @"maps.apple.com";

		if ( [versionNum compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending ) {
			nativeMapScheme = @"maps.google.com";
		}

		NSString *url = [NSString stringWithFormat:@"http://%@/maps?q=%@", nativeMapScheme, where];
		return url;
	}
@end
