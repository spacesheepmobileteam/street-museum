//
// Created by ali kiran on 7/5/13.
// Copyright (c) 2013 Spacesheep. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import <Facebook-iOS-SDK/FacebookSDK/FBSession.h>
#import <Facebook-iOS-SDK/FacebookSDK/FBAccessTokenData.h>
#import <Facebook-iOS-SDK/FacebookSDK/FBRequest.h>
#import <RCLocationManager/RCLocationManager.h>
#import <Foursquare-API-v2/Foursquare2.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "GTSocialManager.h"
#import "GTHudManager.h"
#import "FBWebDialogs.h"
#import "FBError.h"
#import "TWAPIManager.h"
#import "UIActionSheet+BlocksKit.h"
#import "TWSignedRequest.h"
#import "UIAlertView+BlocksKit.h"
#import "UICKeyChainStore.h"
#import "SHKItem.h"
#import "RegexKitLite.h"

@interface GTSocialManager()

	@property(nonatomic, copy, readwrite) NSString              *twitter_oauth_token_secret;
	@property(nonatomic, copy, readwrite) NSString              *twitter_oauth_token;
	@property(nonatomic, strong, readwrite) NSMutableDictionary *twitter_user;
@end

@implementation GTSocialManager {

		ACAccountStore *_accountStore;

		NSString     *_fb_error_reason;
		TWAPIManager *_twapiManager;
		NSArray      *_twitter_accounts;
		NSArray      *_fb_read_permissons;
		NSString     *_gplus_client_id;
		GPPSignIn    *_signIn;
	}

	+ ( GTSocialManager * )instance {
		static GTSocialManager *_instance = nil;

		@synchronized ( self ) {
			if ( _instance == nil) {
				_instance = [[self alloc] init];

			}
		}

		return _instance;
	}

	- ( id )init; {
		self = [super init];
		if ( self ) {

		}

		return self;
	}

	- ( void )setFoursquare_access_token:( NSString * )foursquare_access_token; {

		[[NSUserDefaults standardUserDefaults] setObject:foursquare_access_token
												  forKey:@"FOURSQUARE_ACCESS_TOKEN"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}

	- ( NSString * )twitter_oauth_token; {
		return [UICKeyChainStore stringForKey:twitter_oauth_token service:default_twitter];
	}

	- ( NSString * )foursquare_access_token; {
		return [Foursquare2 isAuthorized] ? [[NSUserDefaults standardUserDefaults]
							stringForKey:@"FOURSQUARE_ACCESS_TOKEN"] : nil;
	}

	- ( NSString * )twitter_oauth_token_secret; {
		return [UICKeyChainStore stringForKey:twitter_oauth_token_secret service:default_twitter];
	}

	- ( NSString * )fb_access_token; {
		return [FBSession activeSession].accessTokenData.accessToken;
	}

	- ( BOOL )isFBAuthorized; {
		return [FBSession activeSession].isOpen;
	}

	- ( BOOL )isTwitterAuthorized; {
		return [UICKeyChainStore stringForKey:twitter_oauth_token service:default_twitter].length > 0;
	}

	- ( void )loginWithTwitter:( void (^)(BOOL ) )pFunction; {
		NSLog(@"");
		_twitter_user = [NSMutableDictionary new];

		if ( ![TWAPIManager hasAppKeys] ) {
			UIAlertView *alert = [[UIAlertView alloc]
											   initWithTitle:@"" message:@"You need to add your Twitter app keys to Info.plist to use this demo.\nPlease see README.md for more info." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
			[alert show];
			pFunction(NO);
		}
		else if ( ![TWAPIManager isLocalTwitterAccountAvailable] ) {
			UIAlertView *alert = [[UIAlertView alloc]
											   initWithTitle:@"" message:@"You must add a Twitter account in Settings.app to use this application" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
			[alert show];
			pFunction(NO);
		}
		else {
			[self obtainAccessToTwitterAccountsWithBlock:^(BOOL granted) {
				dispatch_async(dispatch_get_main_queue(), ^{
					if ( granted ) {
						NSLog(@"");

						_twitter_account = nil;
						if ( _twitter_accounts.count > 0 ) {
							UIActionSheet *actionSheet = [UIActionSheet actionSheetWithTitle:@"Twitter Accounts"];

							for ( ACAccount *acct in _twitter_accounts ) {
								[actionSheet addButtonWithTitle:acct.username handler:^{
									_twitter_account = acct;
									[self __setDefaultTwitterAccount:pFunction];

								}];
							}

							[actionSheet setCancelButtonWithTitle:@"Cancel" handler:^{
								NSLog(@"");
								pFunction(NO);
							}];

//							actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
							id <UIApplicationDelegate> appdelegate = [UIApplication sharedApplication].delegate;
							[actionSheet showInView:appdelegate.window.rootViewController.view];
						}
					}
					else {
						UIAlertView *alert = [[UIAlertView alloc]
														   initWithTitle:@"" message:@"We weren't granted access to the user's accounts" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
						[alert show];
						NSLog(@"");

						pFunction(NO);
					}
				});
			}];
		}

	}

	- ( void )__setDefaultTwitterAccount:( void (^)(BOOL) )pFunction; {
		[UICKeyChainStore setString:_twitter_account.identifier forKey:twitter_identifier service:default_twitter];

		_twitter_user[ @"screen_name" ] = _twitter_account.username;

		NSLog(@"reversing user %@", _twitter_user[ @"screen_name" ] );

		[_twapiManager performReverseAuthForAccount:_twitter_account withHandler:^(NSData *responseData, NSError *error) {
			if ( responseData ) {
				NSString *responseStr = [[NSString alloc]
												   initWithData:responseData encoding:NSUTF8StringEncoding];

				NSLog(@"Reverse Auth process returned: %@", responseStr);

				NSArray  *parts = [responseStr componentsSeparatedByString:@"&"];
				NSString *lined = [parts componentsJoinedByString:@"\n"];


				//(\w*)=([\w-]*)
				NSString *regex_pattern   = @"(\\w*)=([\\w-]*)";
				NSArray  *link_components = [responseStr arrayOfCaptureComponentsMatchedByRegex:regex_pattern options:RKLNoOptions range:( (NSRange) { .location=   0UL, .length=    NSUIntegerMax} ) error:NULL];

				NSLog(@"%@", link_components);

				[link_components enumerateObjectsUsingBlock:^(NSArray *obj, NSUInteger idx, BOOL *stop) {
					if ( obj.count ) {
						NSString *service_name = obj[ 1 ];

						if ( [service_name isEqualToString:@"oauth_token"] ) {
							[UICKeyChainStore setString:obj[ 2 ] forKey:twitter_oauth_token service:default_twitter];
						}

						if ( [service_name isEqualToString:@"oauth_token_secret"] ) {
							[UICKeyChainStore setString:obj[ 2 ] forKey:twitter_oauth_token_secret service:default_twitter];
						}

						if ( [service_name isEqualToString:@"user_id"] ) {
							_twitter_user[ @"user_id" ] = obj[ 2 ];
						}

						if ( [service_name isEqualToString:@"screen_name"] ) {
							_twitter_user[ @"screen_name" ] = obj[ 2 ];
						}

					}
				}];

				NSURL *info_api_path = [[NSURL alloc]
											   initWithString:@"https://api.twitter.com/1.1/users/show.json"];

				if ( !_twitter_user[ @"user_id" ] || !_twitter_user[ @"screen_name" ] ) {
					pFunction(NO);
					return;
				}

				NSDictionary *info_params = @{
						@"user_id"     : _twitter_user[ @"user_id" ],
						@"screen_name" : _twitter_user[ @"screen_name" ]

				};

				id <GenericTwitterRequest> info_request = [_twapiManager requestWithUrl:info_api_path parameters:info_params requestMethod:TWSignedRequestMethodGET];
				[info_request setAccount:_twitter_account];

				[info_request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {

					NSLog(@"");

					if ( !error && responseData ) {
						NSError      *jsonParsingError = nil;
						NSDictionary *user_info        = [NSJSONSerialization JSONObjectWithData:responseData
																						 options:0 error:&jsonParsingError];
						_twitter_user[ @"profile_image_url_normal" ] = user_info[ @"profile_image_url" ];
						NSString *profile_image = user_info[ @"profile_image_url" ];

						NSArray *components = [profile_image componentsSeparatedByString:@"_normal"];
						_twitter_user[ @"profile_image_url" ] = [NSString stringWithFormat:@"%@.jpg", components[ 0 ]];
						_twitter_user[ @"location" ]          = user_info[ @"location" ];
						_twitter_user[ @"description" ]       = user_info[ @"description" ];

						dispatch_async(dispatch_get_main_queue(), ^{
							pFunction(YES);
						});
					} else {
						dispatch_async(dispatch_get_main_queue(), ^{
							pFunction(NO);
						});
					}

				}];

			}
			else {
				NSLog(@"Reverse Auth process failed. Error returned was: %@\n", [error localizedDescription]);
				pFunction(NO);
			}
		}];
	}

	- ( void )obtainAccessToTwitterAccountsWithBlock:( void (^)(BOOL) )block {
		ACAccountType *twitterType = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];

		ACAccountStoreRequestAccessCompletionHandler handler = ^(BOOL granted, NSError *error) {
			if ( granted ) {
				_twitter_accounts = [_accountStore accountsWithAccountType:twitterType];
			}

			block(granted);
		};

		//  This method changed in iOS6. If the new version isn't available, fall back to the original (which means that we're running on iOS5+).
		if ( [_accountStore respondsToSelector:@selector(requestAccessToAccountsWithType:options:completion:)] ) {
			[_accountStore requestAccessToAccountsWithType:twitterType options:nil completion:handler];
		}
		else {
			[_accountStore requestAccessToAccountsWithType:twitterType withCompletionHandler:handler];
		}
	}

	- ( void )loginWithFacebook:( void (^)(BOOL) )callback; {
		_fb_error_reason = nil;
		_facebook_user   = nil;

		if ( ![FBSession activeSession].isOpen ) {//email, user_birthday, or user_location.

			[FBSession openActiveSessionWithReadPermissions:_fb_read_permissons allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
				if ( session.state == FBSessionStateClosed ) {
					NSLog(@"");

				} else {
					NSString *message = error.userInfo[ @"NSLocalizedDescription" ];
					_fb_error_reason = error.userInfo[ FBErrorLoginFailedReason ];

					if ( message.length || _fb_error_reason ) {
						UIAlertView *alertView = [[UIAlertView alloc]
															   initWithTitle:@"" message:message];
						[alertView addButtonWithTitle:@"Ok"];
						[alertView show];
						callback(NO);
					} else {
						[self setDefaultFacebookUser:callback];
					}
				}

			}];
		} else {
			[self setDefaultFacebookUser:callback];
		}

	}

	- ( void )setDefaultFacebookUser:( void (^)(BOOL) )callback; {
		NSLog(@"");
		FBRequest *request = [[FBRequest alloc]
										 initWithSession:[FBSession activeSession] graphPath:@"me?fields=id,name,picture.height(320).width(320).type(large),link,username,gender,email,hometown,birthday" parameters:nil HTTPMethod:nil];
		[request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
			NSString *message = error.userInfo[ @"NSLocalizedDescription" ];
			_fb_error_reason = error.userInfo[ FBErrorLoginFailedReason ];

			if ( message.length || _fb_error_reason ) {
				_facebook_user = nil;
				callback(NO);
			} else {
				_facebook_user = result;
				callback(_facebook_user != nil);
			}
		}];
	}

	- ( void )loginWithGooglePlus:( void (^)(BOOL success) )pFunction; {
		NSLog(@"");

		if ( [[GPPSignIn sharedInstance] authentication] ) {
			// The user is signed in.
			// Perform other actions here, such as showing a sign-out button

			NSLog(@"");
		} else {
			// Perform other actions here
			NSLog(@"");
		}

	}

	- ( void )loginWithFoursquare:( void (^)(BOOL success) )pFunction; {

		if ( [Foursquare2 isAuthorized] ) {
			NSLog(@"");
			pFunction(YES);
		} else {

			[Foursquare2 authorizeWithCallback:^(BOOL success, id result) {
				pFunction(success);
			}];

		}
	}

	- ( void )logoutFromFoursquare; {
		[Foursquare2 removeAccessToken];

	}

	- ( void )logoutFromTwitter; {
		_twitter_user = nil;
		[UICKeyChainStore removeAllItemsForService:default_twitter];

	}

	- ( void )logoutFromFacebook; {
		NSLog(@"");

		[[FBSession activeSession] closeAndClearTokenInformation];
		[FBSession setActiveSession:nil];
	}

	- ( void )shareWithTwitter:( SHKItem * )item callback:( void (^)(BOOL) )callback; {

		RCLocationManager *locationManager = [RCLocationManager sharedManager];
		[locationManager retriveUserLocationWithBlock:^(CLLocationManager *manager, CLLocation *newLocation, CLLocation *oldLocation) {
			[self _update_user_status:item callback:callback location:newLocation];

		}                                  errorBlock:^(CLLocationManager *manager, NSError *error) {
			[self _update_user_status:item callback:callback location:nil ];
		}];

	}

	- ( void )_update_user_status:( SHKItem * )item callback:( void (^)(BOOL) )callback location:( CLLocation * )location; {

		[[GTHudManager instance]
					   showWithTitle:@"" status:@"" afterDelay:0.f];
		NSURL        *info_api_path = [[NSURL alloc]
											  initWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
		NSDictionary *info_params;
		if ( location ) {
			info_params = @{
					@"status" : item.text,

			};
		}
		else {
			info_params = @{
					@"status"              : item.text,
					@"lat"                 : [NSString stringWithFormat:@"%f", location.coordinate.latitude],
					@"long"                : [NSString stringWithFormat:@"%f", location.coordinate.longitude],
					@"display_coordinates" : @"true"

			};
		}

		id <GenericTwitterRequest> info_request = [_twapiManager requestWithUrl:info_api_path parameters:info_params requestMethod:TWSignedRequestMethodPOST];
		[info_request setAccount:_twitter_account];

		[info_request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {

			if ( !error && responseData ) {
				NSError      *jsonParsingError = nil;
				NSDictionary *user_info        = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonParsingError];
				NSLog(@"");
				dispatch_async(dispatch_get_main_queue(), ^{
					[[GTHudManager instance]
								   dismissWithSuccess:@"" title:@"" afterDelay:1.f];
					callback(YES);
				});
			} else {
				dispatch_async(dispatch_get_main_queue(), ^{
					[[GTHudManager instance] dismissWithError:@"" afterDelay:1.f];
					callback(NO);
				});
			}

		}];
	}

	- ( NSDictionary * )parseURLParams:( NSString * )query {
		NSArray             *pairs  = [query componentsSeparatedByString:@"&"];
		NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
		for ( NSString      *pair in pairs ) {
			NSArray  *kv  = [pair componentsSeparatedByString:@"="];
			NSString *val =
							 [kv[ 1 ] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			params[ kv[ 0 ] ] = val;
		}
		return params;
	}

	- ( void )shareWithFacebook:( SHKItem * )item hasUI:( BOOL )hasUI callback:( void (^)(BOOL success) )callback; {

		NSDictionary *action_dict = @{
				@"name" : item.action_name,
				@"link" : item.action_link
		};

		NSData   *json_data = [NSJSONSerialization dataWithJSONObject:action_dict options:0 error:NULL];
		NSString *json_str  = [[NSString alloc] initWithData:json_data encoding:NSUTF8StringEncoding];

		NSMutableDictionary *params =
									[NSMutableDictionary dictionaryWithObjectsAndKeys:
																 item.title,                      @"name",
																 item.text,                       @"caption",
																 item.URL.absoluteString,         @"link",
																 item.facebookURLSharePictureURI, @"picture",
																 json_str,                        @"actions",
																 nil];

		if ( hasUI ) {
			// Invoke the dialog
			[FBWebDialogs presentDialogModallyWithSession:[FBSession activeSession] dialog:@"feed" parameters:params handler:
					^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
						NSLog(@"");
						if ( error ) {
							// Error launching the dialog or publishing a story.
							NSLog(@"Error publishing story.");
//							[[FBSession activeSession] close];
							callback(NO);
						} else {
							if ( result == FBWebDialogResultDialogNotCompleted ) {
								// User clicked the "x" icon
								NSLog(@"User canceled story publishing.");
//								[[FBSession activeSession] close];
								callback(NO);
							} else {
								// Handle the publish feed callback
								NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
								if ( ![urlParams valueForKey:@"post_id"] ) {
									// User clicked the Cancel button
									NSLog(@"User canceled story publishing.");
//									[[FBSession activeSession] close];
									callback(NO);
								} else {
									// User clicked the Share button
									NSString *msg = [NSString stringWithFormat:
																	  @"Posted story, id: %@",
																	  [urlParams valueForKey:@"post_id"]];
									NSLog(@"%@", msg);
									// Show the result in an alert

									if ( error ) {
										//check if user revoked app permissions
										NSDictionary *response = [error.userInfo valueForKey:FBErrorParsedJSONResponseKey];

										NSInteger code     = [[response objectForKey:@"code"]
																		intValue];
										NSInteger bodyCode = [[[[response objectForKey:@"body"]
																		  objectForKey:@"error"]
																		  objectForKey:@"code"]
																		  intValue];

										if ( bodyCode == 190 || code == 403 ) {
											[self logoutFromFacebook];
										} else {
//											[[FBSession activeSession] close];    // unhook us
										}
										callback(NO);
									} else {
										callback(YES);
									}
								}
							}
						}
					}                            delegate:self];
		} else {

			if ( [[FBSession activeSession] isOpen] ) {
				/*
				   * if the current session has no publish permission we need to reauthorize
				   */
				if ( [[[FBSession activeSession] permissions] indexOfObject:@"publish_actions"] == NSNotFound ) {
					[FBSession openActiveSessionWithPublishPermissions:@[ @"publish_actions" ]
													   defaultAudience:FBSessionDefaultAudienceEveryone
														  allowLoginUI:YES
													 completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
														 if ( FBSession.activeSession.isOpen && !error ) {
															 // Publish the story if permission was granted
															 [self publishFBStoryWithParams:params callback:callback];
														 } else {
															 callback(NO);
														 }
													 }];

				} else {
					[self publishFBStoryWithParams:params callback:callback];
				}
			} else {
				/*
				 * open a new session with publish permission
				 */
				[FBSession openActiveSessionWithPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
												   defaultAudience:FBSessionDefaultAudienceEveryone
													  allowLoginUI:YES
												 completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
													 if ( !error && status == FBSessionStateOpen ) {
														 [self publishFBStoryWithParams:params callback:callback];
													 } else {
														 NSLog(@"error");
														 callback(NO);
													 }
												 }];
			}

		}

	}

	- ( void )publishFBStoryWithParams:( NSMutableDictionary * )params callback:( void (^)(BOOL) )callback; {
		NSLog(@"");
		[[GTHudManager instance]
					   showWithTitle:@"" status:@"" afterDelay:0.f];

		[FBRequestConnection startWithGraphPath:@"me/feed"
									 parameters:params
									 HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection,
																			id result,
																			NSError *error) {

			NSString *alertText;
			if ( error ) {
				alertText = [NSString stringWithFormat:@"error: domain = %@, code = %d", error.domain, error.code];
				callback(NO);
			} else {
				alertText = [NSString stringWithFormat:@"Posted action, id: %@", result[ @"id" ]];
				callback(YES);
			}

		}];

	}

	- ( void )logout; {
		[self logoutFromFacebook];
		[self logoutFromTwitter];
		[self logoutFromFoursquare];

	}

	- ( void )shareWithFoursquare:( SHKItem * )item callback:( void (^)(BOOL) )callback; {

		[Foursquare2 checkinAddAtVenue:item.venue_id event:item.event_id shout:item.shout broadcast:item.broadcast latitude:[NSNumber numberWithDouble:item.location.coordinate.latitude] longitude:[NSNumber numberWithDouble:item.location.coordinate.longitude] accuracyLL:[NSNumber numberWithDouble:item.location.horizontalAccuracy] altitude:[NSNumber numberWithDouble:item.location.altitude] accuracyAlt:[NSNumber numberWithDouble:item.location.verticalAccuracy] callback:^(BOOL success, id result) {
			callback(success);
		}];
	}

	- ( NSString * )gplus_client_id {
		if ( !_gplus_client_id ) {
			NSBundle *bundle = [NSBundle mainBundle];
			_gplus_client_id = (NSString *) bundle.infoDictionary[ @"GOOGLE_CLIENT_ID" ];
		}

		return _gplus_client_id;
	}

	- ( void )configure; {
		[Foursquare2 setupFoursquareWithClientId:@"YOUR_KEY"
										  secret:@"YOUR_SECRET"
									 callbackURL:@"YOUR_CALLBACK_URL"];

		_fb_read_permissons = @[ @"user_birthday",
								 @"email",
								 @"user_location" ];
//			[FBSession openActiveSessionWithAllowLoginUI:NO];
		[FBSession openActiveSessionWithReadPermissions:_fb_read_permissons allowLoginUI:NO completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
			NSLog(@"");
			if ( !error ) {
				[self setDefaultFacebookUser:^(BOOL b) {
					NSLog(@"");
				}];
			} else {
				NSLog(@"unable to restore session");
				[self logoutFromFacebook];
			}
		}];

		_twitter_user  = [NSMutableDictionary new];
		_facebook_user = (NSDictionary <FBGraphUser> *) [NSDictionary new];

		_accountStore = [[ACAccountStore alloc] init];
		_twapiManager = [[TWAPIManager alloc] init];

		NSString *def_twitter_identifier = [UICKeyChainStore stringForKey:twitter_identifier service:default_twitter];
		if ( def_twitter_identifier.length ) {
			_twitter_account = [_accountStore accountWithIdentifier:def_twitter_identifier];

			if ( !_twitter_account ) {
				[self logoutFromTwitter];

				UIAlertView *alertView = [[UIAlertView alloc]
													   initWithTitle:@"" message:@"Your twitter connection appears broken, please relogin twitter account"];
				[alertView addButtonWithTitle:@"Reconnect" handler:^{
					[self loginWithTwitter:^(BOOL b) {

					}];
				}];

				[alertView addButtonWithTitle:@"Cancel" handler:^{

				}];

				[alertView show];
			} else {
				[self __setDefaultTwitterAccount:^(BOOL b) {

				}];
			}

		}

		_signIn = [GPPSignIn sharedInstance];
		_signIn.shouldFetchGooglePlusUser  = YES;
		_signIn.shouldFetchGoogleUserEmail = YES;
		_signIn.clientID                   = @"228276079454-dlsqgelta1p6dqedjkpmnaaktsme7drr.apps.googleusercontent.com";

		_signIn.scopes = [NSArray arrayWithObjects:
										  kGTLAuthScopePlusLogin, // defined in GTLPlusConstants.h
										  nil];

		_signIn.delegate = self;


		// Read Google+ deep-link data.
		[GPPDeepLink setDelegate:self];
		[GPPDeepLink readDeepLinkAfterInstall];

		[_signIn trySilentAuthentication];

	}

	- ( void )finishedWithAuth:( GTMOAuth2Authentication * )auth error:( NSError * )error; {
		NSLog(@"");
	}

	- ( void )didReceiveDeepLink:( GPPDeepLink * )deepLink {
		// An example to handle the deep link data.
		UIAlertView *alert = [[UIAlertView alloc]
										   initWithTitle:@"Deep-link Data"
												 message:[deepLink deepLinkID]
												delegate:nil
									   cancelButtonTitle:@"OK"
									   otherButtonTitles:nil];
		[alert show];
	}

	- ( void )didDisconnectWithError:( NSError * )error; {
		NSLog(@"");
	}

@end
