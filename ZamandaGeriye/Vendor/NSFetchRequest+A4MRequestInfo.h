//
// Created by ali kiran on 08/12/13.
// Copyright (c) 2013 ali kiran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFetchRequest(A4MRequestInfo)

	- ( NSMutableDictionary * )predicate_info;
@end
