//
//  UIView+Layout.h
//  FTLibrary
//
//  Created by Simon Lee on 21/12/2009.
//  Copyright 2009 Fuerte International. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface UIView(Layout)

	- ( UIView * )removeSubviews;

	- ( UIView * )positionAtX:( double )xValue;

	- ( UIView * )positionAtY:( double )yValue;

	- ( UIView * )positionAtX:( double )xValue andY:( double )yValue;

	- ( UIView * )positionAtX:( double )xValue andY:( double )yValue withWidth:( double )width;

	- ( UIView * )positionAtX:( double )xValue andY:( double )yValue withHeight:( double )height;

	- ( UIView * )positionAtX:( double )xValue withHeight:( double )height;

	- ( UIView * )centerInSuperView;

	- ( UIView * )aestheticCenterInSuperView;

	- ( UIView * )bringToFront;

	- ( UIView * )sendToBack;

	- ( UIView * )centerAtX;

	- ( UIView * )moveCenterAtHeight:( double )height;

	- ( UIView * )moveCenterAtWidth:( double )width;

	- ( UIView * )centerAtY;

	- ( UIView * )moveYWith:( double )offset;

	- ( UIView * )moveXWith:( double )offset;

	- ( UIView * )centerAtXQuarter;

	- ( UIView * )centerAtX3Quarter;

	- ( UIView * )alignToRight:( double )offset;

	- ( UIView * )alignToBottom:( double )offset;

	@property(nonatomic) CGFloat left;
	@property(nonatomic) CGFloat top;
	@property(nonatomic) CGFloat right;
	@property(nonatomic) CGFloat bottom;
	@property(nonatomic) CGFloat width;
	@property(nonatomic) CGFloat height;
	@property(nonatomic) CGPoint origin;
	@property(nonatomic) CGSize  size;
@end
