//
//  UIView+Layout.m
//  FTLibrary
//
//  Created by Simon Lee on 21/12/2009.
//  Copyright 2009 Fuerte International. All rights reserved.
//

#import "UIView+Layout.h"

@implementation UIView(Layout)

	- ( UIView * )removeSubviews {
		for ( UIView *view in self.subviews ) {
			[view removeFromSuperview];
		}

		return self;
	}

	- ( CGFloat )left {
		return self.frame.origin.x;
	}

	- ( void )setLeft:( CGFloat )x {
		CGRect frame = self.frame;
		frame.origin.x = x;
		self.frame     = frame;
	}

	- ( CGFloat )top {
		return self.frame.origin.y;
	}

	- ( void )setTop:( CGFloat )y {
		CGRect frame = self.frame;
		frame.origin.y = y;
		self.frame     = frame;
	}

	- ( CGFloat )right {
		return self.frame.origin.x + self.frame.size.width;
	}

	- ( void )setRight:( CGFloat )right {
		CGRect frame = self.frame;
		frame.origin.x = right - frame.size.width;
		self.frame     = frame;
	}

	- ( CGFloat )bottom {
		return self.frame.origin.y + self.frame.size.height;
	}

	- ( void )setBottom:( CGFloat )bottom {
		CGRect frame = self.frame;
		frame.origin.y = bottom - frame.size.height;
		self.frame     = frame;
	}

	- ( CGFloat )width {
		return self.frame.size.width;
	}

	- ( void )setWidth:( CGFloat )width {
		CGRect frame = self.frame;
		frame.size.width = width;
		self.frame       = frame;
	}

	- ( CGFloat )height {
		return self.frame.size.height;
	}

	- ( void )setHeight:( CGFloat )height {
		CGRect frame = self.frame;
		frame.size.height = height;
		self.frame        = frame;
	}

	- ( CGPoint )origin {
		return self.frame.origin;
	}

	- ( void )setOrigin:( CGPoint )origin {
		CGRect frame = self.frame;
		frame.origin = origin;
		self.frame   = frame;
	}

	- ( CGSize )size {
		return self.frame.size;
	}

	- ( void )setSize:( CGSize )size {
		CGRect frame = self.frame;
		frame.size = size;
		self.frame = frame;
	}

	- ( UIView * )positionAtX:( double )xValue {
		CGRect frame = [self frame];
		frame.origin.x = (CGFloat) round(xValue);
		[self setFrame:frame];
		return self;
	}

	- ( UIView * )positionAtY:( double )yValue {
		CGRect frame = [self frame];
		frame.origin.y = (CGFloat) round(yValue);
		[self setFrame:frame];
		return self;
	}

	- ( UIView * )positionAtX:( double )xValue andY:( double )yValue {
		CGRect frame = [self frame];
		frame.origin.x = (CGFloat) round(xValue);
		frame.origin.y = (CGFloat) round(yValue);
		[self setFrame:frame];
		return self;
	}

	- ( UIView * )positionAtX:( double )xValue andY:( double )yValue withWidth:( double )width {
		CGRect frame = [self frame];
		frame.origin.x   = (CGFloat) round(xValue);
		frame.origin.y   = (CGFloat) round(yValue);
		frame.size.width = (CGFloat) width;
		[self setFrame:frame];
		return self;
	}

	- ( UIView * )positionAtX:( double )xValue andY:( double )yValue withHeight:( double )height {
		CGRect frame = [self frame];
		frame.origin.x    = (CGFloat) round(xValue);
		frame.origin.y    = (CGFloat) round(yValue);
		frame.size.height = (CGFloat) height;
		[self setFrame:frame];
		return self;
	}

	- ( UIView * )positionAtX:( double )xValue withHeight:( double )height {
		CGRect frame = [self frame];
		frame.origin.x    = (CGFloat) round(xValue);
		frame.size.height = (CGFloat) height;
		[self setFrame:frame];
		return self;
	}

	- ( UIView * )centerInSuperView {
		double xPos = round(( self.superview.frame.size.width - self.frame.size.width ) / 2.0);
		double yPos = round(( self.superview.frame.size.height - self.frame.size.height ) / 2.0);
		[self positionAtX:xPos andY:yPos];
		return self;
	}

	- ( UIView * )aestheticCenterInSuperView {
		double xPos = round(( [self.superview width] - [self width] ) / 2.0);
		double yPos = round(( [self.superview height] - [self height] ) / 2.0) - ( [self.superview height] / 8.0 );
		[self positionAtX:xPos andY:yPos];
		return self;
	}

	- ( UIView * )bringToFront {
		[self.superview bringSubviewToFront:self];
		return self;
	}

	- ( UIView * )sendToBack {
		[self.superview sendSubviewToBack:self];
		return self;
	}

//ZF

	- ( UIView * )centerAtX {
		double xPos = round(( self.superview.width - self.width ) / 2.0);
		[self positionAtX:xPos];
		return self;
	}

	- ( UIView * )moveCenterAtHeight:( double )height {
		double yPos = self.top + round(( height - self.height ) / 2.0);
		[self positionAtY:yPos];
		return self;
	}

	- ( UIView * )moveCenterAtWidth:( double )width {
		double xPos = self.left + round(( width - self.width ) / 2.0);
		[self positionAtX:xPos];
		return self;
	}

	- ( UIView * )centerAtY {
		double yPos = round(( self.superview.height - self.height ) / 2.0);
		[self positionAtY:yPos];
		return self;
	}

	- ( UIView * )moveYWith:( double )offset {
		[self positionAtY:self.top + offset];
		return self;
	}

	- ( UIView * )moveXWith:( double )offset {
		[self positionAtX:self.left + offset];
		return self;
	}

	- ( UIView * )centerAtXQuarter {
		double xPos = round(( self.superview.frame.size.width / 4 ) - ( self.frame.size.width / 2 ));
		[self positionAtX:xPos];
		return self;
	}

	- ( UIView * )centerAtX3Quarter {
		[self centerAtXQuarter];
		double xPos = round(( self.superview.frame.size.width / 2 ) + self.frame.origin.x);
		[self positionAtX:xPos];
		return self;
	}

	- ( UIView * )alignToRight:( double )offset {
		double xPos = round(( self.superview.width - self.width ) + offset);
		[self positionAtX:xPos];
		return self;
	}

	- ( UIView * )alignToBottom:( double )offset {
		double yPos = round(( self.superview.height - self.height ) + offset);
		[self positionAtY:yPos];

		return self;
	}

@end
