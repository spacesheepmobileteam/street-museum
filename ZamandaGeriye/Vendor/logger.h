//
// Created by ali kiran on 02/12/13.
// Copyright (c) 2013 ali kiran. All rights reserved.
//

#import "TestFlight.h"

static int logger (FILE *__restrict __stream, __const char *__restrict __format, ...);

static int logger (FILE *__restrict __stream, __const char *__restrict __format, ...) {
	@autoreleasepool {

		va_list args;

		va_start(args, __format);

		int return_status = 0;

		return_status = vfprintf(__stream, __format, args);

		TFLogv([NSString stringWithUTF8String:__format], args);

		va_end(args);
		return return_status;
	}
}


