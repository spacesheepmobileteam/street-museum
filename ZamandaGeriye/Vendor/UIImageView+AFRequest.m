//
// Created by alikiran on 1/30/13.
//
//


#import <objc/runtime.h>
#import "AFNetworking.h"
#import "UIImageView+AFRequest.h"
#import "TMCache.h"
#import "UIImage+Rightway.h"

static char imageRequestOperationObjectKey;
static char imageURLKey;

@implementation UIImageView(AFRequest)

	- ( AFImageRequestOperation * )setRequestWithURLString:( NSString * )url size:( CGSize )size complete:( void (^)(UIImage *, UIImageView *) )complete {
		if ( !url ) {
			complete(nil, self);
		}

		objc_setAssociatedObject(self, &imageURLKey, url, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

		NSString *cache_key   = [NSString stringWithFormat:@"%@_%dx%d", url, (int) size.width, (int) size.height];
		UIImage  *image_cache = [[TMCache sharedCache]
										  objectForKey:cache_key];

		if ( image_cache ) {
			complete(image_cache, self);
			return nil;
		}

		if ( [self imageRequestOperation] ) {
			[[self imageRequestOperation] cancel];
			[self setImageRequestOperation:nil];
		}

		__block AFImageRequestOperation *af_operation = nil;
		if ( url ) {
			NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[[NSURL alloc]
																			initWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:3.f];
			af_operation = [AFImageRequestOperation imageRequestOperationWithRequest:urlRequest imageProcessingBlock:^UIImage * (UIImage *image) {
				return image;
			}                                                                success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
				if ( af_operation == [self imageRequestOperation] ) {
					image = [self _processImage:image width:(int) size.width height:(int) size.height quality:kCGInterpolationHigh mode:UIViewContentModeCenter];
					[[TMCache sharedCache] setObject:image forKey:cache_key];
					complete(image, self);
				}

			}                                                                failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
				if ( af_operation == [self imageRequestOperation] ) {
					complete(nil, self);
				}

			}];

			[self setImageRequestOperation:af_operation];
//			af_operation.imageScale = 4;
			[af_operation start];
		}
		return af_operation;
	}

	- ( UIImage * )_processImage:( UIImage * )original
				   width:( float )width
				   height:( float )height
				   quality:( CGInterpolationQuality )quality
				   mode:( UIViewContentMode )mode {
		float oh = original.size.height;
		float ow = original.size.width;

		height = height;
		width  = width;

		float nh, nw;

		if ( oh < ow ) {
			float sf = height / oh;
			nh = height;
			nw = ow * sf;
		} else {
			float sf = width / ow;
			nh = oh * sf;
			nw = width;
		}

		//NSLog(@"%f, %f, %f, %f", nw,nh,crop_left,crop_top);

		CGSize  size     = CGSizeMake(nw, nh);
		UIImage *resized = [original resizedImage:size interpolationQuality:quality];

		CGRect  bounds   = CGRectMake(( nw - width ) / 2,
									  ( nh - height ) / 2,
									  width,
									  height);
		UIImage *cropped = [resized croppedImage:bounds];

		return cropped;
	}

	- ( AFImageRequestOperation * )setRequestWithURLString:( NSString * )url complete:( void (^)(UIImage *, UIImageView *) )complete {
		if ( !url ) {
			complete(nil, self);
		}

		objc_setAssociatedObject(self, &imageURLKey, url, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

		UIImage *image_cache = [[TMCache sharedCache] objectForKey:url];

		if ( image_cache ) {
			complete(image_cache, self);
			return nil;
		}

		if ( [self imageRequestOperation] ) {
			[[self imageRequestOperation] cancel];
			[self setImageRequestOperation:nil];
		}

		__block AFImageRequestOperation *af_operation = nil;
		if ( url ) {
			NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[[NSURL alloc]
																			initWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:3.f];
			af_operation = [AFImageRequestOperation imageRequestOperationWithRequest:urlRequest imageProcessingBlock:^UIImage * (UIImage *image) {
				return image;
			}                                                                success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
				if ( af_operation == [self imageRequestOperation] ) {
					[[TMCache sharedCache] setObject:image forKey:url];
					complete(image, self);
				}

			}                                                                failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
				if ( af_operation == [self imageRequestOperation] ) {
					complete(nil, self);
				}

			}];

			[self setImageRequestOperation:af_operation];
//			af_operation.imageScale = 4;
			[af_operation start];
		}
		return af_operation;
	}

	- ( NSString * )imageURL {
		return (NSString *) objc_getAssociatedObject(self, &imageURLKey);
	}

	- ( AFHTTPRequestOperation * )imageRequestOperation {
		return (AFHTTPRequestOperation *) objc_getAssociatedObject(self, &imageRequestOperationObjectKey);
	}

	- ( void )setImageRequestOperation:( AFImageRequestOperation * )imageRequestOperation {
		objc_setAssociatedObject(self, &imageRequestOperationObjectKey, imageRequestOperation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	}

	+ ( NSOperationQueue * )sharedImageRequestOperationQueue {
		static NSOperationQueue *_af_imageRequestOperationQueue = nil;
		static dispatch_once_t  onceToken;
		dispatch_once(&onceToken, ^{
			_af_imageRequestOperationQueue = [[NSOperationQueue alloc] init];
			[_af_imageRequestOperationQueue setMaxConcurrentOperationCount:3];
		});

		return _af_imageRequestOperationQueue;
	}

@end
