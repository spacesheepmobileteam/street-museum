//
// Created by ali kiran on 03/01/14.
// Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import "StubManager.h"
#import "OHHTTPStubs.h"
#import "NSString+ContainsString.h"

@implementation StubManager {

  }

  + ( void )start; {

      //initial seeds
      /*NSData   *stations_data = [NSData dataWithContentsOfFile:genres_path];

      NSDictionary *stations = [NSJSONSerialization JSONObjectWithData:stations_data options:0 error:nil];
      NSMutableArray *m_stations = [NSMutableArray new];

      [stations enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
          [m_stations addObject:@{ @"name" : key, @"value" : obj }];
      }];

      NSData *json_data = [NSJSONSerialization dataWithJSONObject:m_stations options:0 error:nil];
      NSString *json_str = [[NSString alloc] initWithUTF8String:json_data.bytes];*/


      //stub stations call to local json cache
      [OHHTTPStubs stubRequestsPassingTest:^BOOL (NSURLRequest *request) {
          BOOL contains = [request.URL.absoluteString containsString:@"/genres"];
          return contains; // Stub ALL requests without any condition
      } withStubResponse:^OHHTTPStubsResponse * (NSURLRequest *request) {
          // Stub all those requests with "Hello World!" string

          NSString *locations_json = @"locations";
          NSString *locations_path = [[NSBundle mainBundle] pathForResource:locations_json ofType:@"json"];
          NSData   *stations_data  = [NSData dataWithContentsOfFile:locations_path];
          NSArray  *stations       = [NSJSONSerialization JSONObjectWithData:stations_data options:0 error:nil];

          NSUInteger randNum = (NSUInteger) ( rand() % ( 39 - 1 ) + 1 ); //create the random number.


          NSMutableArray *rand_genres = [[NSMutableArray alloc] initWithCapacity:randNum];

          for ( NSUInteger i = 0; i < randNum; i++ ) {
              [rand_genres addObject:stations[ i ]];
          }

          NSData *json_data = [NSJSONSerialization dataWithJSONObject:rand_genres options:0 error:nil];

          NSLog(@"");
          return [OHHTTPStubsResponse responseWithData:json_data statusCode:200
                                                                 headers:@{ @"Content-Type" : @"application/json" }];
      }];

  }
@end
