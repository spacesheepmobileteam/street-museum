//
// Created by ali kiran on 7/5/13.
// Copyright (c) 2013 Spacesheep. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import <Social/Social.h>
#import <Twitter/Twitter.h>
#import <CoreLocation/CoreLocation.h>
#import <GooglePlus/GooglePlus.h>

@protocol FBGraphUser;

@class ACAccount;
@class SHKItem;
@class FBSession;

static NSString *const twitter_identifier         = @"twitter_identifier";
static NSString *const default_twitter            = @"default_twitter";
static NSString *const twitter_oauth_token        = @"twitter_oauth_token";
static NSString *const twitter_oauth_token_secret = @"twitter_oauth_token_secret";

static NSString *const default_facebook = @"default_facebook";

typedef void(^TWAPIHandler) (NSData *data, NSError *error);

@protocol GenericTwitterRequest

	- ( void )performRequestWithHandler:( SLRequestHandler )handler;

	- ( void )setAccount:( ACAccount * )account;

@end

@interface GTSocialManager : NSObject <GPPSignInDelegate, GPPDeepLinkDelegate>

	@property(nonatomic, strong, readonly) NSMutableDictionary *twitter_user;

	@property(nonatomic, copy, readonly) NSString *twitter_oauth_token;

	@property(nonatomic, copy, readonly) NSString *twitter_oauth_token_secret;

	@property(nonatomic, copy, readonly) NSString *foursquare_access_token;

	@property(nonatomic, copy, readonly) NSString *fb_access_token;

	@property(nonatomic, strong) NSDictionary <FBGraphUser> *facebook_user;

	@property(nonatomic, strong) ACAccount *twitter_account;

	@property(nonatomic, strong) NSString *fb_error_reason;

	+ ( GTSocialManager * )instance;

	- ( void )loginWithFacebook:( void (^)(BOOL) )callback;

	- ( void )loginWithFoursquare:( void (^)(BOOL success) )pFunction;

	- ( void )loginWithTwitter:( void (^)(BOOL ) )pFunction;

	- ( void )logoutFromTwitter;

	- ( void )logoutFromFacebook;

	- ( void )logoutFromFoursquare;

	- ( void )shareWithTwitter:( SHKItem * )item callback:( void (^)(BOOL) )callback;

	- ( void )shareWithFacebook:( SHKItem * )item hasUI:( BOOL )hasUI callback:( void (^)(BOOL success) )callback;

	- ( void )logout;

	- ( void )shareWithFoursquare:( SHKItem * )item callback:( void (^)(BOOL) )callback;

	- ( BOOL )isFBAuthorized;

	- ( BOOL )isTwitterAuthorized;

	- ( void )configure;
@end
