//
// Created by ali kiran on 7/6/13.
// Copyright (c) 2013 Spacesheep. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>

@interface GTLocationHelper : NSObject

	+ ( void )openMapsWithDirectionsTo:( CLLocationCoordinate2D )to;

	+ ( void )getTwoLocationDistance:( CLLocationCoordinate2D )to callback:( void (^)(CLLocationDistance) )callback;

	+ ( NSString * )getNativeMapDirection:( CLLocationCoordinate2D )from newLocation:( CLLocationCoordinate2D )to;

	+ ( NSString * )getNativeMapQuery:( NSString * )where;
@end
