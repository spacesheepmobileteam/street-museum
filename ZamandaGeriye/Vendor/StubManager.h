//
// Created by ali kiran on 03/01/14.
// Copyright (c) 2014 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StubManager : NSObject

    + ( void )start;
@end
