//
// Created by ali kiran on 02/12/13.
// Copyright (c) 2013 ali kiran. All rights reserved.
//


#import "NSDictionary+A4MFailSafe.h"

@implementation NSDictionary(A4MFailSafe)

	- ( NSString * )fail_safe_string:( NSString * )string; {
		return [self valueForKey:string] ? self[ string ] : @"";
	}

	- ( NSDictionary * )fail_safe_dictionary:( NSString * )string; {
		return [self valueForKey:string] ? self[ string ] : [NSDictionary new];
	}

	- ( NSArray * )fail_safe_array:( NSString * )string; {
		return [self valueForKey:string] ? self[ string ] : [NSArray new];
	}
@end
