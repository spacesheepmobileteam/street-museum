//
// Created by Serdar Yıllar on 27/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import "UIImage+NewCategory.h"

@implementation UIImage(NewCategory)

	+ ( UIImage * )imageNamed:( NSString * )imageName bundleName:( NSString * )bundleName; {
		if ( !bundleName ) {
			return [UIImage imageNamed:imageName];
		}

		NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
		NSString *bundlePath   = [resourcePath stringByAppendingPathComponent:bundleName];
		NSString *imagePath    = [bundlePath stringByAppendingPathComponent:imageName];
		return [UIImage imageWithContentsOfFile:imagePath];
	}
@end
