//
// Created by ali kiran on 08/12/13.
// Copyright (c) 2013 ali kiran. All rights reserved.
//

#import "NSFetchRequest+A4MRequestInfo.h"

@implementation NSFetchRequest(A4MRequestInfo)

	- ( NSMutableDictionary * )predicate_info; {
		NSMutableDictionary *fetch_variables = [NSMutableDictionary new];

		if ( self.predicate ) {

			//collect predicate parameters
			if ( [self.predicate isKindOfClass:[NSCompoundPredicate class]] ) {


				// get the predicate
				NSCompoundPredicate *predicate = (NSCompoundPredicate *) self.predicate;

				// set the query string values
				for ( NSComparisonPredicate *comparison in predicate.subpredicates ) {

					[fetch_variables setValue:comparison.rightExpression.constantValue forKey:comparison.leftExpression.keyPath];

				}

			} else if ( [self.predicate isKindOfClass:[NSComparisonPredicate class]] ) {


				// get the predicate
				NSComparisonPredicate *predicate = (NSComparisonPredicate *) self.predicate;

				// set the query string values
				[fetch_variables setValue:predicate.rightExpression.constantValue forKey:predicate.leftExpression.keyPath];

			}

		}

		//collect entity user info
		NSDictionary *request_info = self.entity.userInfo;

		[request_info enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
			fetch_variables[ key ] = obj;
		}];

		return fetch_variables;
	}

@end
