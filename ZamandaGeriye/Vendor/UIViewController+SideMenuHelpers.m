//
// Created by ali kiran on 29/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import "UIViewController+SideMenuHelpers.h"
#import "SideMenuController.h"

@implementation UIViewController(SideMenuHelpers)

	- ( SideMenuController * )side_menu {
		return [SideMenuController instance];
	}
@end
