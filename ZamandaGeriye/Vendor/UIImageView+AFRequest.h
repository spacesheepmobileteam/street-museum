//
// Created by alikiran on 1/30/13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@class AFImageRequestOperation;
@class AFHTTPRequestOperation;

@interface UIImageView(AFRequest)

	- ( AFImageRequestOperation * )setRequestWithURLString:( NSString * )url size:( CGSize )size complete:( void (^)(UIImage *, UIImageView *) )complete;

	- ( AFImageRequestOperation * )setRequestWithURLString:( NSString * )url complete:( void (^)(UIImage *, UIImageView *) )complete;

	- ( NSString * )imageURL;

	- ( AFHTTPRequestOperation * )imageRequestOperation;

	- ( void )setImageRequestOperation:( AFImageRequestOperation * )imageRequestOperation;
@end
