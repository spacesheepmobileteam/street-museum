//
// Created by ali kiran on 29/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>

@class SideMenuController;

@interface UIViewController(SideMenuHelpers)

	- ( SideMenuController * )side_menu;
@end
