//
// Created by Serdar Yıllar on 27/11/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface UIImage(NewCategory)

	+ ( UIImage * )imageNamed:( NSString * )imageName bundleName:( NSString * )bundleName;
@end
