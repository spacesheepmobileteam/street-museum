//
//  SelectedPackage.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 13/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "SelectedPackage.h"

@implementation SelectedPackage

	+ ( SelectedPackage * )instance {
		static SelectedPackage *_instance = nil;

		@synchronized ( self ) {
			if ( _instance == nil) {
				_instance = [[self alloc] init];
			}
		}

		return _instance;
	}

	- ( void )setPackage {

	}

	- ( NSMutableDictionary *) getSelectedPackage{
		return _selectedPackages;
	}

@end
