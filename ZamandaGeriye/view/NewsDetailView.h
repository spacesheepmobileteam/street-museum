//
//  NewsDetailView.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "News.h"

@protocol PhotoBrowserDelegate;
@class DTAttributedTextView;
@class RTLabel;
@class CMHTMLView;
@class Location;

@interface NewsDetailView : UIView <UICollectionViewDelegate, UICollectionViewDataSource, UIWebViewDelegate>

	@property(nonatomic, weak) id <PhotoBrowserDelegate>   delegate;

	@property(weak, nonatomic) IBOutlet UIButton                   *btn_locate_at_map;
	@property(weak, nonatomic) IBOutlet UICollectionView           *cv_album;
	@property(weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flw_layout;
	@property(weak, nonatomic) IBOutlet UIPageControl              *page_control;
	@property(weak, nonatomic) IBOutlet UILabel                    *lbl_title;
	@property(weak, nonatomic) IBOutlet NSLayoutConstraint         *cns_webview_height;
	@property(weak, nonatomic) IBOutlet UILabel                    *lbl_distance;
	@property(weak, nonatomic) IBOutlet UIButton                   *btn_share_twitter;
	@property(weak, nonatomic) IBOutlet UIButton                   *btn_share_facebook;
	@property(weak, nonatomic) IBOutlet UIButton                   *btn_share_instagram;
	@property(weak, nonatomic) IBOutlet CMHTMLView                 *cm_html_view;
	@property(weak, nonatomic) IBOutlet UIWebView                  *web_view;
	@property(weak, nonatomic) IBOutlet UILabel                    *lbl_top;
	@property(weak, nonatomic) IBOutlet UIScrollView               *scr_scroll_view;
	@property(weak, nonatomic) IBOutlet UILabel                    *lbl_bottom;
	@property(weak, nonatomic) IBOutlet UIButton                   *btn_like;
	@property(weak, nonatomic) IBOutlet UILabel                    *lbl_like_count;

	@property(nonatomic, strong) News *news_detail;

	@property(nonatomic, strong) Location *location_detail;

    @property(nonatomic, strong) News *selected_data;
@end
