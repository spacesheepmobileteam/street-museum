//
//  NewsView.m
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "NewsView.h"
#import "NewsViewCell.h"
#import "NewsViewDelegate.h"
#import "News.h"
#import "UIImageView+AFRequest.h"

@implementation NewsView { NSDateFormatter *new_date_formatter; }

	- ( id )initWithFrame:( CGRect )frame {
		self = [super initWithFrame:frame];
		if ( self ) {
			// Initialization code
		}
		return self;
	}

	- ( void )awakeFromNib; {
	}

	- ( void )tableView:( UITableView * )tableView didSelectRowAtIndexPath:( NSIndexPath * )indexPath; {

	}

	- ( NSInteger )tableView:( UITableView * )tableView numberOfRowsInSection:( NSInteger )section {

		return [_delegate getNewsCount:self];
	}

	- ( NSInteger )numberOfSectionsInTableView:( UITableView * )tableView; {
		return 1;
	}

	- ( UITableViewCell * )tableView:( UITableView * )tableView cellForRowAtIndexPath:( NSIndexPath * )indexPath {

		static NSString *CellIdentifier = @"Cell";
		NewsViewCell    *cell           = [_tbl_news dequeueReusableCellWithIdentifier:CellIdentifier];
		News            *news_data      = [_delegate getNewsItem:indexPath.row at:self];

		cell.lbl_date.text  = [self.dateFormatter stringFromDate:news_data.create_date];
		cell.lbl_desc.text  = news_data.spot_text;
		cell.lbl_title.text = news_data.title;

		[cell.img_thumb setRequestWithURLString:news_data.thumb
									   complete:^(UIImage *image, UIImageView *imageView) {
										   dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
											   forceImageDecompression(image);

											   dispatch_async(dispatch_get_main_queue(), ^{
												   imageView.image = image;
											   });
										   });
									   }];

		return cell;
	}

	- ( NSDateFormatter * )dateFormatter {
		if ( new_date_formatter != nil) {
			return new_date_formatter;
		}

		new_date_formatter = [[NSDateFormatter alloc] init];
		[new_date_formatter setDateStyle:NSDateFormatterShortStyle];
		[new_date_formatter setTimeStyle:NSDateFormatterShortStyle];
		return new_date_formatter;
	}

	- ( void )reload; {

		[_tbl_news reloadData];

	}
@end
