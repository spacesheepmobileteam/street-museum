//
//  FilterView.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 11/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterView : UIView <UITableViewDelegate, UITableViewDataSource>

    @property(weak, nonatomic) IBOutlet UITableView *tbl_filter;
    @property(weak, nonatomic) IBOutlet UIButton    *btn_clear;
    @property(weak, nonatomic) IBOutlet UIButton    *btn_filter;
    @property(weak, nonatomic) IBOutlet UIButton    *btn_all;
    @property(weak, nonatomic) IBOutlet UIButton    *btn_near_by;

    - ( void )reload;
@end
