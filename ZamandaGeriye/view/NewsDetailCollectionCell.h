//
//  NewsDetailCollectionCell.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_thumb;

@end
