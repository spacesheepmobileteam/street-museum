//
//  PlacesMapView.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 02/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "PlacesMapView.h"
#import "LocationAnnotation.h"
#import "PlacesMapViewDelegate.h"
#import "LocationAnnotationView.h"
#import "LocationCalloutAnnotation.h"
#import "LocationCalloutAnnotationView.h"
#import "Location.h"

@implementation PlacesMapView {
		LocationCalloutAnnotation *activeCalloutAnnotation;
		id <MKAnnotation>         activeAnnotation;
	}

	- ( id )initWithFrame:( CGRect )frame {
		self = [super initWithFrame:frame];
		if ( self ) {
			// Initialization code
		}
		return self;
	}

	- ( void )awakeFromNib; {

		_mk_map.delegate = self;

		MKUserLocation     *userLocation = _mk_map.userLocation;
		MKCoordinateRegion region        =
								   MKCoordinateRegionMakeWithDistance(
										   userLocation.location.coordinate, 20000, 20000);
		[_mk_map setRegion:region animated:NO];

	}

	- ( void )mapView:( MKMapView * )mapView didUpdateUserLocation:( MKUserLocation * )userLocation; {
		_mk_map.centerCoordinate =
				userLocation.location.coordinate;
	}

	- ( MKAnnotationView * )mapView:( MKMapView * )mapView viewForAnnotation:( id <MKAnnotation> )annotation {
		static int added = 0;

		if (/* added == 0 &&*/ [annotation isKindOfClass:[LocationAnnotation class]] ) {
			static NSString *const kAnnotationIdentifier = @"BlipMapAnnotation";

			LocationAnnotationView *annotationView = (LocationAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:kAnnotationIdentifier];

			if ( !annotationView ) {
				annotationView = [[LocationAnnotationView alloc]
														  initWithAnnotation:annotation
															 reuseIdentifier:kAnnotationIdentifier];
				annotationView.enabled = YES;
//				annotationView.canShowCallout = YES;
//				annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeInfoLight];
			}
			[annotationView setAnnotation:annotation];

//			added++;
			return annotationView;
		} else if (/* added == 0 &&*/ [annotation isKindOfClass:[LocationCalloutAnnotation class]] ) {

			static NSString *const kAnnotationIdentifier = @"BlipCalloutMapAnnotation";


			/*LocationAnnotationCalloutView *annotationView = (LocationAnnotationCalloutView *) [mapView dequeueReusableAnnotationViewWithIdentifier:kAnnotationIdentifier];*/

			LocationAnnotationCalloutView *annotationView = [[LocationAnnotationCalloutView alloc]
																							initWithAnnotation:annotation
																							   reuseIdentifier:kAnnotationIdentifier];
			annotationView.enabled        = YES;
			annotationView.canShowCallout = YES;

			if ( !annotationView ) {
				annotationView = [[LocationAnnotationCalloutView alloc]
																 initWithAnnotation:annotation
																	reuseIdentifier:kAnnotationIdentifier];
				annotationView.enabled        = YES;
				annotationView.canShowCallout = YES;
//				annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeInfoLight];
			}
			[annotationView setAnnotation:annotation];

//			added++;
			return annotationView;

		}


		/*else {
			MKPinAnnotationView *annotationView = [[MKPinAnnotationView alloc]
																		initWithAnnotation:annotation reuseIdentifier:@"bişi"];
			return annotationView;
		}*/



		return nil;
	}

	- ( void )reloadWithSpecificLocation:( Location * )sender_location {

		[_mk_map reloadInputViews];
		[_mk_map removeAnnotations:_mk_map.annotations];
		MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };

		LocationAnnotation *custom_annotation = [[LocationAnnotation alloc]
																	 initWithCoordinate:CLLocationCoordinate2DMake(sender_location.latitude.doubleValue, sender_location.longitude.doubleValue)
																			andLocation:sender_location];


//																				 initWithCoordinate:CLLocationCoordinate2DMake(41.037154, 28.993765) andLocation:location];

		[_mk_map addAnnotation:custom_annotation];
		[self zoomToFitMapAnnotations];

	}

	- ( void )reload; {
		[_mk_map reloadInputViews];

		[_mk_map removeAnnotations:_mk_map.annotations];

		MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };

		[[_delegate getAnnotationArray]
					enumerateObjectsUsingBlock:^(Location *location, NSUInteger idx, BOOL *stop) {

						NSLog(@"");

						LocationAnnotation *custom_annotation = [[LocationAnnotation alloc]
																					 initWithCoordinate:CLLocationCoordinate2DMake(location.latitude.doubleValue, location.longitude.doubleValue)
																							andLocation:location];


//																				 initWithCoordinate:CLLocationCoordinate2DMake(41.037154, 28.993765) andLocation:location];

						[_mk_map addAnnotation:custom_annotation];



						/*if ( idx == 1 ) {
							*stop = YES;
						}*/

					}];

		[self zoomToFitMapAnnotations];

	}

	- ( void )mapView:( MKMapView * )mapView didSelectAnnotationView:( MKAnnotationView * )view; {

		NSLog(@"");

		/*	if (self.calloutAnnotation == nil) {
				[LocationCalloutAnnotation new];
			}
			else {
				self.calloutAnnotation.latitude = view.annotation.coordinate.latitude;
				self.calloutAnnotation.longitude = view.annotation.coordinate.longitude;
			}*/

		LocationCalloutAnnotation *calloutAnnotation = [[LocationCalloutAnnotation alloc]
																				   initWithCoordinate:view.annotation.coordinate
																						  andLocation:( (LocationAnnotation *) view.annotation ).location];


//		calloutAnnotation.coordinate = view.annotation.coordinate;


		[[NSNotificationCenter defaultCenter]
							   postNotificationName:@"selectedPlacesMapItem"
											 object:( (LocationAnnotation *) view.annotation ).location];

		activeCalloutAnnotation = calloutAnnotation;
		activeAnnotation        = view.annotation;

//		[_mk_map selectAnnotation:calloutAnnotation animated:NO];

		[_mk_map addAnnotation:calloutAnnotation];
//

	}

	- ( void )mapView:( MKMapView * )mapView didDeselectAnnotationView:( MKAnnotationView * )view {
		NSLog(@" ");

		if ( activeCalloutAnnotation && view.annotation == activeAnnotation ) {
			[_mk_map removeAnnotation:activeCalloutAnnotation];

		}
	}

	- ( void )zoomToFitMapAnnotations {

		if ( [_mk_map.annotations count] == 0 ) {
			return;
		}

		CLLocationCoordinate2D topLeftCoord;
		topLeftCoord.latitude  = -90;
		topLeftCoord.longitude = 180;

		CLLocationCoordinate2D bottomRightCoord;
		bottomRightCoord.latitude  = 90;
		bottomRightCoord.longitude = -180;

		for ( LocationAnnotation *annotation in _mk_map.annotations ) {
			topLeftCoord.longitude     = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
			topLeftCoord.latitude      = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
			bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
			bottomRightCoord.latitude  = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
		}

		MKCoordinateRegion region;
		region.center.latitude    = topLeftCoord.latitude - ( topLeftCoord.latitude - bottomRightCoord.latitude ) * 0.5;
		region.center.longitude   = topLeftCoord.longitude + ( bottomRightCoord.longitude - topLeftCoord.longitude ) * 0.5;
		region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.1;

		// Add a little extra space on the sides
		region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.1;

		// Add a little extra space on the sides
		region = [_mk_map regionThatFits:region];
		[_mk_map setRegion:region animated:YES];

	}

@end
