//
//  LocationAnnotation.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 03/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "LocationAnnotation.h"
#import "Location.h"

@interface LocationAnnotation()

	@property(nonatomic, readwrite, copy) NSString *title;
@end

@implementation LocationAnnotation

	- ( id )initWithCoordinate:( CLLocationCoordinate2D )coordinate andLocation:( Location * )location {
		self = [super init];

		if ( self != nil) {
			self.location   = location;
			self.coordinate = coordinate;
			self.title      = self.location.title;
		}

		return self;
	}

@end
