//
//  CalloutDetail.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 04/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Location;

@interface CalloutDetail : UIView

	@property(weak, nonatomic) IBOutlet UILabel *lbl_title;
	@property(nonatomic, weak) Location        *location_data;

	- ( void )reload;
@end
