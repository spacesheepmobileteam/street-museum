//
// Created by Serdar Yıllar on 03/12/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import "LocationAnnotationView.h"
#import "LocationCalloutAnnotationView.h"
#import "CalloutDetail.h"
#import "UIView+Layout.h"
#import "Location.h"

@implementation LocationAnnotationCalloutView {

  }

  - ( id )initWithAnnotation:( id <MKAnnotation> )annotation reuseIdentifier:( NSString * )reuseIdentifier {
      self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
      if ( self ) {
          CGRect        frame        = [self frame];
          NSArray       *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CalloutDetail" owner:self options:nil];
          CalloutDetail *myView      = [nibContents objectAtIndex:0];

          myView.location_data = self.location_data;
          [myView reload];

          [self addSubview:myView];

          frame.size = myView.size;

          self.frame        = frame;
          self.centerOffset = CGPointMake(0, -myView.height / 2 - 38);

      }
      return self;
  }

  - ( Location * )location_data {
      return self.annotion_data.location;
  }

  - ( LocationAnnotation * )annotion_data {
      return ( (LocationAnnotation *) self.annotation );
  }

@end
