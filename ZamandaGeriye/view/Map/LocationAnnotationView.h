//
// Created by Serdar Yıllar on 03/12/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "LocationAnnotation.h"

@interface LocationAnnotationView : MKAnnotationView

    - ( Location * )location_data;

    - ( LocationAnnotation * )annotion_data;
@end
