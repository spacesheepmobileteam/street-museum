//
//  PlacesMapView.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 02/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@protocol PlacesMapViewDelegate;

@interface PlacesMapView : UIView <MKMapViewDelegate>

	@property(weak, nonatomic) IBOutlet MKMapView *mk_map;
	@property(weak, nonatomic) IBOutlet UIButton  *btn_show_all;

	@property(nonatomic, weak) id <PlacesMapViewDelegate> delegate;

	- ( void )reload;
@end
