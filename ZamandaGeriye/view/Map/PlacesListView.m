//
//  PlacesListView.m
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "PlacesListView.h"
#import "PlacesListViewDelegate.h"
#import "PlacesListCellView.h"
#import "LocationListHeaderView.h"
#import "Location.h"
#import "LocationType.h"

@implementation PlacesListView

  - ( id )initWithFrame:( CGRect )frame {
      self = [super initWithFrame:frame];
      if ( self ) {
          // Initialization code
      }
      return self;
  }

  - ( void )awakeFromNib; {

      _tbl_list.delegate   = self;
      _tbl_list.dataSource = self;
      [_tbl_list reloadData];

  }

  - ( UIView * )tableView:( UITableView * )tableView viewForHeaderInSection:( NSInteger )section {

      LocationListHeaderView *locationListHeaderView = [[[NSBundle mainBundle]
                                                                   loadNibNamed:@"LocationListHeaderView" owner:self
                                                                        options:nil] objectAtIndex:0];

      Location *location = [_delegate getGroupedLocationsArray:section at:self][ 0 ];

      LocationType *location_type = location.types.anyObject;
      locationListHeaderView.lbl_section_title.text = location_type.title;

      return locationListHeaderView;
  }

  - ( void )tableView:( UITableView * )tableView didSelectRowAtIndexPath:( NSIndexPath * )indexPath; {
//		NSLog(@"%f",[(Locations *) [_delegate getLocationsItem:indexPath.row at:self] internalBaseClassIdentifier]);
  }

  - ( CGFloat )tableView:( UITableView * )tableView heightForHeaderInSection:( NSInteger )section; {
      return 40;
  }

  - ( NSInteger )tableView:( UITableView * )tableView numberOfRowsInSection:( NSInteger )section {

      return [_delegate getGroupedLocationsArray:section at:self].count;
  }

  - ( NSInteger )numberOfSectionsInTableView:( UITableView * )tableView; {
      return [_delegate getGroupedLocationsCount:self];
  }

  - ( UITableViewCell * )tableView:( UITableView * )tableView cellForRowAtIndexPath:( NSIndexPath * )indexPath {

      static NSString *CellIdentifier = @"Cell";

      PlacesListCellView *cell = [_tbl_list dequeueReusableCellWithIdentifier:CellIdentifier];
      cell.delegate = self;

//		Locations *location = [_delegate getLocationsItem:indexPath.row at:self];
      NSMutableArray *groupedArray = [_delegate getGroupedLocationsArray:indexPath.section at:self];

      Location *location = groupedArray[ (NSUInteger) indexPath.row ];

      cell.lbl_description.text = location.desc;
      cell.lbl_title.text       = location.title;

      LocationType *location_type = location.types.anyObject;

      cell.lbl_tag_name.text = location_type.title;
      cell.tag               = indexPath.row;
      cell.location          = location;
      return cell;
  }

  - ( void )callLocateAtMapWithId:( int )cellIndex; {

      [_tbl_list selectRowAtIndexPath:[NSIndexPath indexPathForRow:cellIndex inSection:0] animated:NO
                       scrollPosition:UITableViewScrollPositionNone];
      [_delegate openLocationOnMapWithCellIndex:cellIndex];

  }

  - ( void )reload; {
      _tbl_list.frame = self.frame;
      [_tbl_list reloadData];

  }

@end
