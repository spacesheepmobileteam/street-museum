//
//  PlacesListView.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlacesListCellViewDelegate.h"

@protocol PlacesListViewDelegate;

@interface PlacesListView : UIView <UITableViewDelegate, UITableViewDataSource, PlacesListCellViewDelegate>

    @property(weak, nonatomic) IBOutlet UITableView *tbl_list;
    @property(weak, nonatomic) IBOutlet UIView      *cnt_container_view;

    @property(weak, nonatomic) IBOutlet UILabel            *lbl_message;
    @property(nonatomic, weak) id <PlacesListViewDelegate> delegate;

    - ( void )reload;

@end
