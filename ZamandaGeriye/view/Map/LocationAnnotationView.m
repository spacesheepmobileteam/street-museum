//
// Created by Serdar Yıllar on 03/12/13.
// Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//


#import "LocationAnnotationView.h"
#import "Location.h"

@implementation LocationAnnotationView {

  }

  - ( id )initWithAnnotation:( id <MKAnnotation> )annotation reuseIdentifier:( NSString * )reuseIdentifier {
      self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
      if ( self ) {
          UIImage *blipImage = [UIImage imageNamed:@"type-22"];
          CGRect  frame      = [self frame];
          frame.size = [blipImage size];
          self.frame = frame;

//			self.backgroundColor = [UIColor redColor];
          self.image = blipImage;

          self.centerOffset = CGPointMake(0, -20);



          /*[self setFrame:frame];
          [self setCenterOffset:CGPointMake(0.0, 0.0)];
          [self setImage:blipImage];*/
      }
      return self;
  }

  - ( Location * )location_data {
      return self.annotion_data.location;
  }

  - ( LocationAnnotation * )annotion_data {
      return ( (LocationAnnotation *) self.annotation );
  }

@end
