//
//  LocationListHeaderView.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 11/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationListHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIButton *btn_view_item;

@property (weak, nonatomic) IBOutlet UIButton *btn_buy_item;

@property (weak, nonatomic) IBOutlet UILabel *lbl_section_title;
@end
