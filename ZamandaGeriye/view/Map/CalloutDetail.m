//
//  CalloutDetail.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 04/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "CalloutDetail.h"
#import "Location.h"

@implementation CalloutDetail

	- ( id )initWithFrame:( CGRect )frame {
		self = [super initWithFrame:frame];
		if ( self ) {
			// Initialization code
		}
		return self;
	}

	- ( IBAction)tap_view_btn:( id )sender {

	}

	- ( IBAction)tap_how_to_go_btn:( id )sender {
		NSLog(@"");
	}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

	- ( void )reload; {
		_lbl_title.text = _location_data.title;
	}
@end
