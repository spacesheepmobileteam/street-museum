//
//  LocationAnnotation.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 03/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <MapKit/MKAnnotation.h>

@class Location;

@interface LocationCalloutAnnotation : NSObject <MKAnnotation> {
	}

	@property(nonatomic, assign) CLLocationCoordinate2D coordinate;
	@property(nonatomic, readonly, copy) NSString       *title;
	@property(nonatomic, readonly, copy) NSString       *subtitle;

	@property(nonatomic, strong) Location *location;

	- ( id )initWithCoordinate:( CLLocationCoordinate2D )coordinate andLocation:( Location * )location;
@end
