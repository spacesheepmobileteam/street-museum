//
//  NewsView.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NewsViewDelegate;

@interface NewsView : UIView <UITableViewDelegate, UITableViewDataSource>

	@property (weak, nonatomic) IBOutlet UITableView *tbl_news;
	@property(nonatomic, weak) id <NewsViewDelegate> delegate;

	- ( void )reload;
@end
