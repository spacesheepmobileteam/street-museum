//
//  FilterViewCell.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 11/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "ProductViewCell.h"
#import "LocationType.h"
#import "Product.h"
#import "UIImageView+AFRequest.h"
#import "City.h"

@implementation ProductViewCell

  - ( id )initWithStyle:( UITableViewCellStyle )style reuseIdentifier:( NSString * )reuseIdentifier {
      self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
      if ( self ) {
          // Initialization code

      }
      return self;
  }

  - ( IBAction)action_view_item:( id )sender {
  }

  - ( void )setSelected_data:( Product * )selected_data; {
      _selected_data = selected_data;
      _lbl_desc.text  = selected_data.desc;
      _lbl_title.text = selected_data.name;

      City     *city       = selected_data.cities.anyObject;
      NSString *thumb_path = city.visual;

      [_img_thumb setRequestWithURLString:thumb_path complete:^(UIImage *image, UIImageView *view) {
          view.image = image;

      }];
  }

  - ( NSString * )reuseIdentifier; {
      return [self.class reuseIdentifier];
  }

  + ( NSString * )reuseIdentifier; {
      return NSStringFromClass(self);
  }

  - ( void )updateConstraints; {
      [super updateConstraints];

      if ( self.didSetupConstraints ) {
          return;
      }

      self.didSetupConstraints = YES;
  }

  - ( void )prepareForReuse {
      NSLog(@"");
  }

  - ( BOOL )shouldIndentWhileEditing; {
      return NO;
  }

  - ( CGFloat )indentationWidth; {
      return 0;
  }

  - ( NSInteger )indentationLevel; {
      return 0;
  }

  - ( void )didMoveToWindow {
      [self layoutIfNeeded];

      if ( self.window ) {
      }
  }

  - ( void )willTransitionToState:( UITableViewCellStateMask )aState {
      [super willTransitionToState:aState];
      self.state = aState;
  }

  - ( void )setSelected:( BOOL )selected animated:( BOOL )animated {
      [super setSelected:selected animated:animated];
      // Configure the view for the selected state
  }

@end
