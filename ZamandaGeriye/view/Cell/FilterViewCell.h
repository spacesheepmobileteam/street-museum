//
//  FilterViewCell.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 11/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LocationType;

@interface FilterViewCell : UITableViewCell

    @property(weak, nonatomic) IBOutlet UIButton *btn_filter_tick;
    @property(weak, nonatomic) IBOutlet UILabel  *lbl_filter_tag;

    @property(nonatomic) UITableViewCellStateMask state;
    @property(nonatomic) BOOL                     didSetupConstraints;
    @property(nonatomic, strong) LocationType     *location_type;

    + ( NSString * )reuseIdentifier;
@end
