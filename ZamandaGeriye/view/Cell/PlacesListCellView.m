//
//  PlacesListCellView.m
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "PlacesListCellView.h"
#import "MainModel.h"
#import "PlacesListCellViewDelegate.h"

MainModel *main_model;

@implementation PlacesListCellView

	- ( id )initWithStyle:( UITableViewCellStyle )style reuseIdentifier:( NSString * )reuseIdentifier {
		self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
		if ( self ) {
			// Initialization code
		}

		return self;
	}

	- ( void )setSelected:( BOOL )selected animated:( BOOL )animated {
		[super setSelected:selected animated:animated];
	}

	- ( IBAction)click_locate_at_map:( id )sender {
		[_delegate callLocateAtMapWithId:self.tag];
	}

	- ( void )awakeFromNib; {


/*
	[_btn_like addEventHandler:^(id sender) {

	} forControlEvents:UIControlEventTouchUpInside];



	[_btn_locate_at_map addEventHandler:^(id sender) {

	} forControlEvents:UIControlEventTouchUpInside];
*/


	}

@end
