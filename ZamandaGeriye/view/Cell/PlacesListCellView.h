//
//  PlacesListCellView.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PlacesListCellViewDelegate;
@class Location;

@interface PlacesListCellView : UITableViewCell

    @property(nonatomic, weak) id <PlacesListCellViewDelegate> delegate;

    @property(weak, nonatomic) IBOutlet UIButton *btn_locate_at_map;
    @property(weak, nonatomic) IBOutlet UILabel  *lbl_tag_name;
    @property(weak, nonatomic) IBOutlet UIButton *btn_like;
    @property(weak, nonatomic) IBOutlet UILabel  *lbl_like_count;
    @property(weak, nonatomic) IBOutlet UILabel  *lbl_description;
    @property(weak, nonatomic) IBOutlet UILabel  *lbl_title;

    @property(nonatomic, strong) Location *location;
@end
