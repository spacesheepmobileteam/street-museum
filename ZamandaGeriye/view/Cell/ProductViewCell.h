//
//  FilterViewCell.h
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 11/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LocationType;
@class Product;

@interface ProductViewCell : UITableViewCell

    @property(weak, nonatomic) IBOutlet UILabel *lbl_item_status;

    @property(nonatomic) UITableViewCellStateMask state;
    @property(nonatomic) BOOL                     didSetupConstraints;
    @property(nonatomic, strong) Product          *selected_data;
    @property(weak, nonatomic) IBOutlet UILabel   *lbl_title;

    @property(weak, nonatomic) IBOutlet UILabel     *lbl_desc;
    @property(weak, nonatomic) IBOutlet UIImageView *img_thumb;
    + ( NSString * )reuseIdentifier;
@end
