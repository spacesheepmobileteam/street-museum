//
//  CityVerticalView.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 12/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CityCarouselViewDelegate;
@class City;

@interface CityVerticalView : UIView

    @property(weak, nonatomic) IBOutlet UILabel              *lbl_title;
    @property(weak, nonatomic) IBOutlet UILabel              *lbl_description;
    @property(weak, nonatomic) IBOutlet UILabel              *lbl_like_count;
    @property(weak, nonatomic) IBOutlet UIButton             *btn_like;
    @property(weak, nonatomic) IBOutlet UIButton             *btn_share;
    @property(weak, nonatomic) IBOutlet UIButton             *btn_view_item;
    @property(weak, nonatomic) IBOutlet UIButton             *btn_buy_item;
    @property(weak, nonatomic) IBOutlet UIImageView          *img_thumb;
    @property(nonatomic, weak) id <CityCarouselViewDelegate> delegate;

    @property(nonatomic, strong) City *selected_data;
@end
