//
//  CityHorizontalView.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 12/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CityCarouselViewDelegate;
@class City;

@interface CityHorizontalView : UIView

    @property(weak, nonatomic) IBOutlet UIImageView *img_thumb;
    @property(weak, nonatomic) IBOutlet UILabel     *lbl_title;
    @property(weak, nonatomic) IBOutlet UILabel     *lbl_description;
    @property(weak, nonatomic) IBOutlet UILabel     *lbl_like_count;
    @property(weak, nonatomic) IBOutlet UIButton    *btn_like;
    @property(weak, nonatomic) IBOutlet UIButton    *btn_share;
    @property(weak, nonatomic) IBOutlet UIButton    *btn_view_item;
    @property(weak, nonatomic) IBOutlet UIButton    *btn_buy_item;

    @property(weak, nonatomic) IBOutlet UILabel *lbl_places_count;
    @property(weak, nonatomic) IBOutlet UILabel *lbl_photos_count;
    @property(weak, nonatomic) IBOutlet UILabel *lbl_Legends_count;
    @property(weak, nonatomic) IBOutlet UILabel *lbl_artist_count;
    @property(weak, nonatomic) IBOutlet UILabel *lbl_news_count;

    @property(nonatomic, strong) id <CityCarouselViewDelegate> delegate;
    @property(nonatomic, strong) City *selected_data;
@end
