//
//  NewsViewCell.m
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "NewsViewCell.h"
#import "News.h"
#import "UIImageView+AFRequest.h"

@implementation NewsViewCell

	- ( id )initWithStyle:( UITableViewCellStyle )style reuseIdentifier:( NSString * )reuseIdentifier {
		self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
		if ( self ) {
			// Initialization code
		}
		return self;
	}

  - ( void )setSelected_data:( News * )selected_data; {
      _selected_data = selected_data;
      NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
      [formatter setDateFormat:@"yyyy"];
      [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];

      NSString *stringFromDate = [formatter stringFromDate:_selected_data.create_date];

      _lbl_date.text = stringFromDate;
      _lbl_desc.text  = selected_data.spot_text;
      _lbl_title.text = selected_data.title;

      NSString *thumb_path = selected_data.thumb;

      [_img_thumb setRequestWithURLString:thumb_path complete:^(UIImage *image, UIImageView *view) {
          view.image = image;
      }];
  }


	- ( void )awakeFromNib; {

		_img_thumb.layer.masksToBounds = YES;
		_img_thumb.layer.cornerRadius  = 5.0;

	}

	+ ( NSString * )identifier; {
		return NSStringFromClass(self);
	}



	- ( void )setSelected:( BOOL )selected animated:( BOOL )animated {
		[super setSelected:selected animated:animated];

		// Configure the view for the selected state
	}

@end
