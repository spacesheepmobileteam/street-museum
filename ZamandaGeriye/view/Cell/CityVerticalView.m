//
//  CityVerticalView.m
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 12/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "CityVerticalView.h"
#import "CityCarouselViewDelegate.h"
#import "City.h"
#import "UIImageView+AFRequest.h"

@implementation CityVerticalView

  - ( IBAction )action_view_item:( id )sender {
      NSLog(@"");
      [_delegate action_view_item_at:self];
  }

  - ( IBAction)action_buy_item:( id )sender {
      [_delegate action_buy_item_at:self];
  }

  - ( void )setSelected_data:( City * )selected_data; {
      _selected_data = selected_data;

      _lbl_title.text       = _selected_data.title;
      _lbl_description.text = _selected_data.visual;
      _lbl_like_count.text  = [NSString stringWithFormat:@"%@", _selected_data.like_count];

      _lbl_title.text       = _selected_data.title;
      _lbl_description.text = _selected_data.desc;

      _btn_buy_item.hidden  = [_delegate hasPurchased:_selected_data];
      _btn_view_item.hidden = !_btn_buy_item.hidden;

      [_img_thumb setRequestWithURLString:_selected_data.visual complete:^(UIImage *image, UIImageView *imageView) {
          dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
              forceImageDecompression(image);

              dispatch_async(dispatch_get_main_queue(), ^{
                  imageView.image = image;
              });
          });
      }];
  }

@end
