//
//  FilterViewCell.m
//  ZamandaGeriye
//
//  Created by Serdar Yıllar on 11/12/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "FilterViewCell.h"
#import "LocationType.h"

@implementation FilterViewCell

  - ( id )initWithStyle:( UITableViewCellStyle )style reuseIdentifier:( NSString * )reuseIdentifier {
      self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
      if ( self ) {
          // Initialization code
      }
      return self;
  }

  - ( IBAction)action_check_filter:( id )sender {
  }

  - ( void )setLocation_type:( LocationType * )location_type; {
      _location_type = location_type;

      _btn_filter_tick.selected = location_type.selected.boolValue;
      _lbl_filter_tag.text      = location_type.title;

  }

  - ( NSString * )reuseIdentifier; {
      return [self.class reuseIdentifier];
  }

  + ( NSString * )reuseIdentifier; {
      return NSStringFromClass(self);
  }

  - ( void )updateConstraints; {
      [super updateConstraints];

      if ( self.didSetupConstraints ) {
          return;
      }

      self.didSetupConstraints = YES;
  }

  - ( void )prepareForReuse {
      NSLog(@"");
  }

  - ( BOOL )shouldIndentWhileEditing; {
      return NO;
  }

  - ( CGFloat )indentationWidth; {
      return 0;
  }

  - ( NSInteger )indentationLevel; {
      return 0;
  }

  - ( void )didMoveToWindow {
      [self layoutIfNeeded];

      if ( self.window ) {
      }
  }

  - ( void )willTransitionToState:( UITableViewCellStateMask )aState {
      [super willTransitionToState:aState];
      self.state = aState;
  }

  - ( void )setSelected:( BOOL )selected animated:( BOOL )animated {
      [super setSelected:selected animated:animated];
      // Configure the view for the selected state
  }

@end
