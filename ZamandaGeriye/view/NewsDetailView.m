//
//  NewsDetailView.m
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 14/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import "NewsDetailView.h"
#import "NewsDetailCollectionCell.h"
#import "IDMPhoto.h"
#import "IDMPhotoBrowser.h"
#import "PhotoBrowserDelegate.h"
#import "MainModel.h"
#import "CMHTMLView.h"
#import "UIView+Layout.h"
#import "GTLocationHelper.h"
#import "Location.h"
#import "Media.h"
#import "UIImageView+AFRequest.h"

MainModel *main_model;

@implementation NewsDetailView {
      NSMutableArray  *photos;
      NSString        *_htmlString;
      NSArray         *_media;
      NSDateFormatter *new_date_formatter;
      NSArray         *media_data;
  }

  - ( void )awakeFromNib; {
      photos = [NSMutableArray new];
      _flw_layout.scrollDirection              = UICollectionViewScrollDirectionHorizontal;
      _flw_layout.collectionView.pagingEnabled = YES;

  }

/*

  - ( void )setNews_detail:( News * )news_detail; {
      _news_detail = news_detail;

      _lbl_title.width = 292;

      NSString *str  = _news_detail.text;
      NSData   *data = [str dataUsingEncoding:NSUTF8StringEncoding];
      _htmlString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

      [_web_view loadHTMLString:_htmlString baseURL:nil];

      _lbl_like_count.hidden = YES;
      _lbl_top.hidden        = YES;
      _lbl_bottom.hidden     = NO;
      _btn_like.hidden       = YES;

      _lbl_bottom.text = [self.dateFormatter stringFromDate:_news_detail.create_date];
      _lbl_title.text  = _news_detail.title;
      [_lbl_title sizeToFit];

      [self reload];

      _media = _news_detail.media.allObjects;

  }
*/

  - ( void )setSelected_data:( News * )selected_data; {

      _lbl_like_count.hidden    = YES;
      _lbl_top.hidden           = YES;
      _lbl_bottom.hidden        = NO;
      _btn_like.hidden          = YES;
      _btn_locate_at_map.hidden = YES;

      _lbl_title.text  = selected_data.title;
      _lbl_title.width = 292;
      [_lbl_title sizeToFit];

      _lbl_bottom.text = [self.dateFormatter stringFromDate:selected_data.create_date];;

      _web_view.delegate = self;
      NSString *desc_str           = selected_data.text;
      NSData   *desc_str_as_NSData = [desc_str dataUsingEncoding:NSUTF8StringEncoding];

      NSString *desc_as_htmlString = [[NSString alloc] initWithData:desc_str_as_NSData encoding:NSUTF8StringEncoding];
      [_web_view loadHTMLString:desc_as_htmlString baseURL:nil];

      media_data = [Media MR_findAllSortedBy:@"identifier" ascending:YES
                                                           withPredicate:[NSPredicate predicateWithFormat:@"ANY news.identifier == %@",
                                                                                                          selected_data.identifier]];
//      media_data = [selected_data.media allObjects];

      if ( media_data.count ) {

          [media_data enumerateObjectsUsingBlock:^(Media *media, NSUInteger idx, BOOL *stop) {

              IDMPhoto *photo = [IDMPhoto photoWithURL:[[NSURL alloc] initWithString:media.source]];
              [photos addObject:photo];

          }];
          [_cv_album reloadData];
      }

  }

/*
	- ( void )setLocation_detail:( Location * )location_detail; {
		_location_detail = location_detail;

		_cm_html_view.fontFamily = @"Helvetica-Neue";
		_cm_html_view.fontSize   = 12.0f;

		_lbl_bottom.hidden     = YES;
		_lbl_top.hidden        = NO;
		_btn_like.hidden       = NO;
		_lbl_like_count.hidden = NO;

		_lbl_title.width = 182;
		_lbl_title.text  = _location_detail.title;

		NSString *str  = _location_detail.text;
		NSData   *data = [str dataUsingEncoding:NSUTF8StringEncoding];

		_htmlString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

		CLLocationCoordinate2D coordinate2D = CLLocationCoordinate2DMake(_location_detail.latitude.doubleValue, _location_detail.longitude.doubleValue);

		[GTLocationHelper getTwoLocationDistance:coordinate2D callback:^(CLLocationDistance d) {

			double distance = round(d);

			if ( distance > 1000 ) {
				int kmDistance = (int) ( distance / 1000 );

				_lbl_top.text = [NSString stringWithFormat:@"%i km uzaktasınız", kmDistance];
			} else if ( distance < 1000 ) {
				_lbl_top.text = [NSString stringWithFormat:@"%i m uzaktasınız", (int) distance];
			}

		}];

		_media = _location_detail.media.allObjects;

		[self reload];
	}*/

  - ( NSDateFormatter * )dateFormatter {
      if ( new_date_formatter != nil) {
          return new_date_formatter;
      }

      new_date_formatter = [[NSDateFormatter alloc] init];
      [new_date_formatter setDateStyle:NSDateFormatterShortStyle];
      [new_date_formatter setTimeStyle:NSDateFormatterShortStyle];
      return new_date_formatter;
  }

  - ( void )webViewDidFinishLoad:( UIWebView * )webView; {
      _cns_webview_height.constant = webView.scrollView.contentSize.height;

  }

  - ( void )collectionView:( UICollectionView * )collectionView didSelectItemAtIndexPath:( NSIndexPath * )indexPath; {

      IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
      if ( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
          browser.modalPresentationCapturesStatusBarAppearance = NO;
      }
      [browser setInitialPageIndex:(NSUInteger) _page_control.currentPage];
      [[UIApplication sharedApplication] setStatusBarHidden:NO];
      [self.window.rootViewController presentViewController:browser animated:NO completion:^{

      }];

  }

  - ( NSInteger )collectionView:( UICollectionView * )collectionView numberOfItemsInSection:( NSInteger )section; {
      _page_control.numberOfPages = media_data.count;
      return media_data.count;
  }

  - ( UICollectionViewCell * )collectionView:( UICollectionView * )collectionView
                      cellForItemAtIndexPath:( NSIndexPath * )indexPath; {

      NewsDetailCollectionCell *newsDetailCollectionCell = [_cv_album dequeueReusableCellWithReuseIdentifier:@"NewsDetailCell"
                                                                      forIndexPath:indexPath];

      Media *media = media_data[ (NSUInteger) indexPath.row ];

      [newsDetailCollectionCell.img_thumb setRequestWithURLString:media.source size:CGSizeMake(320.f, 200.f)
                                                                               complete:^(UIImage *image, UIImageView *view) {
                                                                                   newsDetailCollectionCell.img_thumb.image = image;
                                                                               }];

      return newsDetailCollectionCell;
  }

  - ( void )scrollViewDidEndDecelerating:( UIScrollView * )scrollView {
      CGFloat pageWidth = _cv_album.frame.size.width;
      _page_control.currentPage = (NSInteger) ( _cv_album.contentOffset.x / pageWidth );
  }

@end
