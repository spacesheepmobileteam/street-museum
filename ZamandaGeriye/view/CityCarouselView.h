//
//  CityCarouselView.h
//  Zamanda Geriye
//
//  Created by Serdar Yıllar on 12/11/13.
//  Copyright (c) 2013 Serdar Yıllar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@protocol CityCarouselViewDelegate;
@class City;

@interface CityCarouselView : UIView <iCarouselDelegate, iCarouselDataSource>

    @property(weak, nonatomic) IBOutlet iCarousel            *carousel;
    @property(nonatomic, weak) id <CityCarouselViewDelegate> cityCarouselViewDelegate;
    @property(weak, nonatomic) IBOutlet UIButton             *btn_carousel;
    @property(weak, nonatomic) IBOutlet UIButton             *btn_list;
    @property(weak, nonatomic) IBOutlet NSLayoutConstraint   *cns_toggle_view_vertical_space;

    - ( void )reload;
@end
